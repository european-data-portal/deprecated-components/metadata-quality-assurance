# Metadata Quality Assurance

Monitoring tool for metadata quality.

## Installation for development

### Prerequisite

Install all of the following software:

 * Oracle / OpenJDK JDK = 8, 9
 * Apache Maven >= 3.2
 * PostgreSQL >= 9.2
 * JBoss WildFly (Java EE7 Full & Web Distribution) = 10.x
 * GIT >= 1.9.4 
 * Unix based OS
 
The following additional software will be installed during this guide:

* Phantom JS
* Gradle
 
### Clone the sources
 
Run the following command to clone the MQA repository:

    git clone -b release https://gitlab.com/european-data-portal/metadata-quality-assurance.git

### Set up your system

First, initialize a PostgreSQL database ([Tutorial](https://www.linode.com/docs/databases/postgresql/how-to-install-postgresql-relational-databases-on-centos-7/)).
Log in as user postgres and set the password for the database user postgres:

```
su - postgres
psql -c "ALTER USER postgres WITH PASSWORD 'postgres'" -d template1
```

Now change the allowed socket connection interfaces which are set in the `pg_hba.conf` file. For Debian, this file is found in `/etc/postgresql/your_version/main/` and in CentOS at `/var/lib/pgsql/data/`. Make sure to run this command with the proper permissions. Adjust the IPv4 settings, changing the `METHOD` to md5 and replacing `samehost` with your localhost IP. In my case this was `127.0.0.1/32` :
```
# TYPE  DATABASE  USER  ADDRESS  METHOD

# "local" is for Unix domain socket connections only
local  all  all  md5
 
# IPv4 local connections:
host  all     postgres  samehost  md5
host  all     nuxeo     samehost  md5
host  cspace  cspace    samehost  md5
```

Then, set up a new user for your wildfly:

```
cd /wildfly_root_dir/bin/
./add-user.sh
```

Create a new Management User by selecting the appropiate option. Enter your username and set a password. In the next steps, ignore WildFly prompting you for possible groups by leaving everything blank and when asked about `AS` connections type `no`. You should now have successfully added a user. 

Now, download the postgres JDBC driver. Information on what version you should be getting can be found [here](https://jdbc.postgresql.org/download.html).

Assuming you are still in the `bin` directory, start up your WildFly server and run the config script (in a separate terminal):
```
./standalone.sh
./jboss-cli.sh
```

Once connected (`connect`), issue the following commands, replacing paths and credentials with your own:

```
module add --name=org.postgres --resources=/path/to/your/jdbc/driver.jar --dependencies=javax.api,javax.transaction.api
/subsystem=datasources/jdbc-driver=postgres:add(driver-name="postgres",driver-module-name="org.postgres",driver-class-name=org.postgresql.Driver)
data-source add --jndi-name=java:jboss/postgresqlDS-MQA --name=PostgrePool --connection-url=jdbc:postgresql://localhost/postgres --driver-name=postgres --user-name=postgres --password=postgres
```

Next, browse to `localhost:9990` and login with the credentials set when creating a user. Navigate to `Configuration -> Subsystem -> EE` and click `Services`, then click `Executor` in the left menu.
Select the one named default and set the following values:
    
|Key|Value|
|:--- |:---|
|Core Threads| 40 |
|Hung Task Threshold| 60000 |
|JNDI Name| java:jboss/ee/concurrency/executor/mqa |
|Long Running Tasks| true |
|Max Threads| 80 |

Click `save` and answer the server reload prompt with `yes`.

### Install dataset-similarities dependency

Run the following commands:

    git clone -b release https://gitlab.com/european-data-portal/dataset-similarities.git
    cd dataset-similarities
    mvn clean install

### Set up URL Checker

For performance reasons the MQA checks the URLs of all datasets for availability using an external service. This service must be set up and its address configured in the `settings.xml` file.
Installation instructions can be found here: [https://gitlab.com/european-data-portal/mqa-url-checker](https://gitlab.com/european-data-portal/mqa-url-checker)

### Set up Metric Cache

For performance reasons the MQA stores precomputed metrics using a dedicated service. This service must be set up and its address configured in the `settings.xml` file.
Installation instructions can be found here: [https://gitlab.com/european-data-portal/mqa-metric-service](https://gitlab.com/european-data-portal/mqa-metric-service)

### Set up PhantomJS for generating charts for report

For the PDF report generation to work, a highcharts export service must have been set up. Running a lightweight PhantomJS server for this task 
will be sufficient. The following steps will guide through the installation process.

First, download PhantomJS from the [official site](http://phantomjs.org/download.html). Then, extract the archive into a directory of your choice:

    bzip2 -dk /path/to/phantomjs.tar.bz2
    tar -xf /path/to/phantomjs.tar
    
Next, either clone or download the official Highcharts export server:

    git clone https://github.com/highcharts/highcharts-export-server.git
    
In there, you'll find a folder named `phantomjs`. Copy the contents of this folder into the `bin` folder of the PhantomJS directory.

    cp -r /path/to/highcharts-export-server/phantomjs/* /path/to/phantomjs/bin/

In this `bin` folder there should now be a file called `resources.json`. Replace it's contents with the string `{"files": "highcharts.js"}`.

    echo '{"files": "highcharts.js"}' > /path/to/phantomjs/bin/resources.json

Due to licence issues the Highcharts files need to be downloaded separately from [this site](http://www.highcharts.com/download/). 
You can either grab the whole package or download files individually. The only file required goes by the name of `highcharts.js` and can be found in the code directory.
Like previously, copy this file into the `bin` folder.

    cp /path/to/highcharts.js /path/to/phantomjs/bin

Finally, enter the `bin` directory and run the server:
    
    cd /path/to/phantomjs/bin
    ./phantomjs highcharts-convert.js -host 127.0.0.1 -port 3003

Don't forget to add the address you have chosen to the MQA configuration file. Make sure to precede the IP address with `http://`.

### Configuration
Create or edit your Maven settings in ~/.m2/settings.xml. Insert the following XML, adjusting the values as desired:

    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
       http://maven.apache.org/xsd/settings-1.0.0.xsd">
    
         <localRepository>${user.home}/.m2/repository</localRepository>
            <interactiveMode>true</interactiveMode>
            <usePluginRegistry>false</usePluginRegistry>
            <offline>false</offline>
        
            <profiles>
                <profile>
                    <id>mqa</id>
                    <properties>
                        <wildfly-hostname>localhost</wildfly-hostname>
                        <wildfly-port>9990</wildfly-port>
                        <wildfly-username>USERNAME</wildfly-username>
                        <wildfly-password>PASSWORD</wildfly-password>
                        <validation.schema.json.url>JSON SCHEMA FILE URL</validation.schema.json.url>
                        <ckan.check.url>URL</ckan.check.url>
                        <ckan.browse.url>URL</ckan.browse.url>
				        <ckan.basicauth.username>OPTIONAL</ckan.basicauth.username>
				        <ckan.basicauth.password>OPTIONAL</ckan.basicauth.password>
                        <hibernate.hbm2ddl.auto>[create-drop|update]</hibernate.hbm2ddl.auto>
                        <jira.url>URL</jira.url>
                        <triplestore.url>URL</triplestore.url>
                        <triplestore.graph>http://europe.eu/paneodp/</triplestore.graph>
                        <triplestore.reference.uri>http://publicdata.eu</triplestore.reference.uri>
                       	<dashboard.url.localhost>http://127.0.0.1:8080/mqa-service/en/dashboard.html</dashboard.url.localhost><!-- May differ in port -->
                        <dcatap_dataclient.page_limit>5</dcatap_dataclient.page_limit>
	                    <timer.validation.interval>0 16 13 ? * Fri</timer.validation.interval>
				        <timer.similarity.interval>0 16 13 ? * Fri</timer.similarity.interval>
                        <url.checker.connectiontimeout>10</url.checker.connectiontimeout>
                        <url.checker.sockettimeout>10</url.checker.sockettimeout>
                        <javax.faces.PROJECT_STAGE>Development</javax.faces.PROJECT_STAGE>
                        <distribution.format.machinereadable>cdf,csv,csv.zip,esri shapefile,geojson,iati,ical,ics,json,kml,kmz,netcdf,nt,ods,psv,psv.zip,rdf,rdfa,rss,shapefile,shp,shp.zip,sparql,sparql web form,tsv,ttl,wms,xlb,xls,xls.zip,xlsx,xml,xml.zip</distribution.format.machinereadable>
                        <harvester.url.rest>URL</harvester.url.rest>
						<harvester.basicauth.username>USERNAME</harvester.basicauth.username>
						<harvester.basicauth.password>PASSWORD</harvester.basicauth.password>
				        <report.directory>PATH TO STORE THE REPORT FILES</report.directory>
				        <report.languages>de,en,fr</report.languages>
				        <maintenance>true</maintenance>
				        <highcharts.url>URL</highcharts.url>
				        <admin.token>mySecret</admin.token>
				        <fingerprint.dir>PATH TO FINGERPRINT DIRECTORY</fingerprint.dir>
				        <fingerprint.schedule>PATH TO SCHEDULE XML-FILE</fingerprint.schedule>
				        <fingerprint.url>URL</fingerprint.url>
				        <machinereadableformats.url>https://gitlab.com/european-data-portal/machine-readable-formats/raw/release/formats.json</machinereadableformats.url>
                        <url.check.endpoint>URL</url.check.endpoint>
                        <host.url>http://127.0.0.1:8080/mqa-service</host.url>
                        <metric.cache.url>URL</metric.cache.url>
                    </properties>
                </profile>
            </profiles>
        
            <activeProfiles>
                <activeProfile>mqa</activeProfile> <!-- This ensures that maven is using the mqa profile with each run -->
            </activeProfiles>
    </settings>
    
A table explaining the required settings is shown below. All other settings can safely be ignored or left at default. 

|Key|Description|
|:--- |:---|
|wildfly-hostname| Adress of the wildfly server |
|wildfly-port| Port of the wildfly server |
|wildfly-username| Management user configured for the wildfly server |
|wildfly-username| Management password configured for the wildfly server |
|validation.schema.json.url| URL where a validation schema can be found (JSON) |
|ckan.check.url| URL of the CKAN instance from which data can be fetched |
|hibernate.hbm2ddl.auto| Hibernate strategy for handling database. Should be set to `create` on first launch, to `validate` on subsequent launches |
|timer.validation.interval| Cron schedule at which a validation run will be initiated |
|harvester.*| If a Harvester is used, the MQA can display data if the connection is specified using these keys |
|report.directory| Folder into which report whiles will be saved. Must be created before first launch|
|report.languages|Lowercase abbreviations of the languages for which reports shall be generated|
|highcharts.url|Adress under which the PhantomJS server used for chart generation is available|
|admin.token|The secret which must be provided for every admin API call|
|fingerprint.dir|Folder into which fingerprint dumps will be saved. Must be created before first launch |
|fingerprint.schedule|Path to file specifiying the fingerprint schedule|
|fingerprint.url|Adress used for fingerprint matching|
|url.check.endpoint|Adress where the MQA URL checker is available|
|metric.cache.url|Adress where the MQA metric service is available|


To allow frequent automated fingerprinting, a schedule may be configured like so:

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <schedules>
         <schedule>
           <languages>de,en</languages>
           <interval>0 45 17 ? * Fri</interval>
         </schedule>
         <schedule>
           <languages>fr,es</languages>
           <interval>0 33 17 ? * Fri</interval>
         </schedule>
    </schedules>

Apart from common, two-lettered language code, the `languages` tag takes two special values. Two underscores `__` indicate the fingerprinting of all pan-european catalogs, and an asterisk `*` serves as a wildcard for all available languages.
The `<interval>` tag requires a cron schedule. Add as many `<schedule>` tags as you like and save the resulting XML-file. Make sure to specify it's path in either the `settings.xml` or `mqa-service.properties` configuration file.
Please note however, that any language can only be scheduled once and will be overwritten by succeeding schedules. 


### Get it running
Start your WildFly server, change into the metadata-quality-assurance root directory and run the following command:

    mvn clean wildfly:deploy
    
### See it running

Open your preferred web browser and go to: 

    http://localhost:8080/mqa-service/en/dashboard.html
    
## Usage of the REST API

### Public

The REST API gives you raw information about the data that is stored in the MQA. It follows the REST principles.

* List of catalogs

        http://mqa-url/mqa-service/api/catalog
        
* Catalog by name

        http://mqa-url/mqa-service/api/catalog/{catalogName}
        
* Dataset names of catalog

        http://mqa-url/mqa-service/api/catalog/{catalogName}/dataset
    
* All datasets (with paging)

        http://mqa-url/mqa-service/api/dataset?start=0&rows=100

* Dataset by name

        http://mqa-url/mqa-service/api/dataset/{datasetName}
    
* Similarities for dataset (with limit)

        http://mqa-url/mqa-service/api/similarity/{datasetId}?limit=10
    

### Admin only

_Note:_ For any of the admin API end points to work an `admin.token` must have been configured in the `settings.xml`.
This token must be passed in JSON notation in the request's body like so:

```json
{"secret":"mySecret"}
```

Also, all end points expect POST methods.    

#### Validation run
In addition to the schedule configured in the `settings.xml` file, a run can be triggered manually. The schedule is not affected by this. 
Only a single run can take place at any given time, so attempting to trigger a second run won't have an affect. The call is as follows:

    http://mqa-url/mqa-service/api/admin/run
    
#### Deletion run
A deletion run can only be started if currently neither a validation nor a deletion run is running.
To start a deletion run, use the following request:

    http://mqa-url/mqa-service/api/admin/delete    
  

#### Refresh known licences
A list of licences is fetched from the EDP licencing assistant, which are then stored as 'known licences'. 
To refresh this list use the following URL:

    http://mqa-url/mqa-service/api/admin/refreshLicences    

#### Fingerprint languages
Fingerprinting languages can be requested via API. The languages have to passed as a comma separated list following the `language` URL query parameter. 
If this parameter is omitted, all languages available will be fingerprinted.
Please note that the languages will not be scheduled. Instead, the fingerprinting will only be attempted once for each language requested. 
If a fingerprinting job for the requested language is already running the request will be lost. A sample call is shown below:

    http://mqa-url/mqa-service/api/admin/fingerprint?languages=de,en        


## Additional information
Depending on the amount of datasets on a portal, the MQA may create a huge number of HTTP requests for checking datasets. If that created too much load on servers, they can try to handle those requests in a more suitable way. In order to indicate a MQA request, the MQA has the following term in its agent:
    
    European Data Portal
