package de.fhg.fokus.edp.mqa.service.config;

/**
 * Created by bdi on 13/04/15.
 */
public interface Constants {

    // scheduler keys
    String SCHEDULER_VALIDATION_JOB = "validationJob";
    String SCHEDULER_VALIDATION_JOB_INSTANT = "validationJobInstant";
    String SCHEDULER_VALIDATION_TRIGGER = "validationTrigger";
    String SCHEDULER_VALIDATION_TRIGGER_INSTANT = "validationTriggerInstant";
    String SCHEDULER_VALIDATION_GROUP = "validationGroup";
    String SCHEDULER_DELETION_JOB = "deletionJob";
    String SCHEDULER_DELETION_JOB_INSTANT = "deletionJobInstant";
    String SCHEDULER_DELETION_TRIGGER = "deletionTrigger";
    String SCHEDULER_DELETION_TRIGGER_INSTANT = "deletionTriggerInstant";
    String SCHEDULER_DELETION_GROUP = "deletionGroup";
    String SCHEDULER_DATASET_SIMILARITY_JOB = "datasetSimilarityJob";
    String SCHEDULER_REPORT_JOB = "schedulerReportJob";
    String SCHEDULER_REPORT_JOB_INSTANT = "schedulerReportJobInstant";
    String SCHEDULER_REPORT_GROUP = "schedulerReportGroup";
    String SCHEDULER_REPORT_TRIGGER = "schedulerReportTrigger";
    String SCHEDULER_REPORT_TRIGGER_INSTANT = "schedulerReportTriggerInstant";
    String SCHEDULER_DATASET_SIMILARITY_TRIGGER = "datasetSimilarityTrigger";
    String SCHEDULER_DATASET_SIMILARITY_GROUP = "datasetSimilarityGroup";
    String SCHEDULER_CACHE_REFRESH_JOB = "cacheRefreshJob";
    String SCHEDULER_CACHE_REFRESH_TRIGGER = "cacheRefreshTrigger";
    String SCHEDULER_CACHE_REFRESH_GROUP = "cacheRefreshGroup";
    String SCHEDULER_FINGERPRINTING_JOB = "FP_JOB_";
    String SCHEDULER_FINGERPRINTING_TRIGGER = "FP_TRIGGER_";
    String SCHEDULER_FINGERPRINTING_GROUP = "FP_GROUP";

    // system config
    String CACHE_NAME_DASHBOARD = "dashboard";
    String MSG_BUNDLE = "lang.mqa";
    String[] DATE_FORMATS = {"yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm:ssZ", "yyyy-MM-dd HH:mm:ss Z",
            "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd", "dd.MM.yyy"};
    String CACHE_REFRESH_CRON_SCHEDULE = "0 0 8 1/1 * ? *";
    String JOB_LANG_KEY = "language_codes";
    String JOB_RUN_ONLY_ONCE = "run_once";
    String LANG_CODE_DELIMITER = ",";
    String LANGUAGE_WILDCARD = "*";
    int LANGUAGE_RETRY_WAIT_TIME_IN_HOURS = 1;
    int BAR_CHART_ELEMENT_COUNT = 20;


    // persistence
    String HINT_FETCH_GRAPH = "javax.persistence.fetchgraph";
    String HINT_LOAD_GRAPH = "javax.persistence.loadgraph";

    // json keys
    String CATALOG_NAME = "name";
    String CATALOG_TITLE = "title";
    String CATALOG_SPATIAL = "spatial";
    String CATALOG_DESCRIPTION = "description";
    String CATALOG_MODIFIED = "created";
    String CATALOG_PUBLISHER = "publisher";
    String PUBLISHER_NAME = "name";
    String PUBLISHER_EMAIL = "email_address";
    String DATASET_NAME = "name";
    String DATASET_DISTRIBUTIONS = "resources";
    String DATASET_VIOLATIONS = "violations";
    String DATASET_TITLE = "title";
    String DATASET_LAST_CHECK_DATE = "last_check_date";
    String DATASET_ID = "id";
    String DATASET_LICENCE_ID = "license_id";
    String DATASET_LICENCE_TITLE = "license_title";
    String DISTRIBUTION_ACCESS_URL = "access_url";
    String DISTRIBUTION_DOWNLOAD_URL = "download_url";
    String DISTRIBUTION_FORMAT = "format";
    String DISTRIBUTION_MIMETYPE = "mimetype";
    String CKAN_OBJECT_INSTANCE_ID = "id";
    String LICENCE_ID = "id";
    String LICENCE_TITLE = "title";
    String LICENCE_STATUS = "status";
    String LICENCE_MAINTAINER = "maintainer";
    String LICENCE_URL = "url";
    String LICENCE_FAMILY = "family";
    String LICENCE_OD_CONFORMANCE = "od_conformance";
    String LICENCE_OSD_CONFORMANCE = "osd_conformance";
    String LICENCE_DOMAIN_DATA = "domain_data";
    String LICENCE_DOMAIN_CONTENT = "domain_content";
    String LICENCE_DOMAIN_SOFTWARE = "domain_software";
    String LICENCE_OKD_COMPLIANCE = "is_okd_compliant";
    String LICENCE_OSI_COMPLIANCE = "is_osi_compliant";
    String LICENCE_IS_GENERIC = "is_generic";

    // not in use
    String COLOR_GOOD = "99A129";
    String COLOR_BAD = "CD203C";


    // error codes
    int ERROR_BAD_URL = 1000;
    int ERROR_NO_URL = 0;
    int ERROR_URL_TOO_LONG = 1010;
    int ERROR_UNKNOWN_HOST = 1020;
    int ERROR_SSL_EXCEPTION = 1030;
    int ERROR_REFUSED = 1040;
    int ERROR_NO_HTTP_RESPONSE = 1050;
    int ERROR_REQUEST_TIMEOUT = 1060;
    int ERROR_SOCKET_TIMEOUT = 1070;
    int ERROR_CONNECT_TIMEOUT = 1080;
    int ERROR_CLIENT_PROTOCOL = 1090;
    int ERROR_ILLEGAL_ARGUMENT = 1100;
    int ERROR_CONNECTION_REFUSED = 1110;
    int ERROR_CONNECTION_RESET = 1120;
    int OK = 200;
}
