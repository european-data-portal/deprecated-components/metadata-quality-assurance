package de.fhg.fokus.edp.mqa.service.utils;

import com.google.common.base.Joiner;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.fetch.MqaHttpProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import javax.json.JsonString;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bdi on 22/01/16.
 */
@ApplicationScoped
@Named
public class Formats {

    private Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Inject
    private MqaHttpProvider mqaHttpProvider;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.MACHINE_READABLE_FORMATS_URL)
    private String machineReadableFormatsUrl;

    private List<String> formats;

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        loadFormats();
    }

    private void loadFormats() {
        formats = new ArrayList<>();
        try {
            JsonObject jsonFormats = mqaHttpProvider.getRemoteFormats(new URL(machineReadableFormatsUrl));
            jsonFormats.getJsonArray("machine_readable").getValuesAs(JsonString.class).forEach(format -> formats.add(format.getString()));
        } catch (MalformedURLException e) {
            LOG.error("Malformed URL for formats URL: {}", machineReadableFormatsUrl);
        }

        String printableFormats = Joiner.on(", ").join(formats);
        LOG.info("Available machine readable formats: " + printableFormats);
    }

    /**
     * Checks if the given format is machine readable.
     *
     * @param format Format to check as String
     * @return Returns TRUE if the given format is machine readable
     */
    public boolean isFormatMachineReadable(String format) {
        return this.formats.stream().anyMatch(formatS -> StringUtils.equalsIgnoreCase(format, formatS));
    }

    /**
     * Gets formats.
     *
     * @return the formats
     */
    public List<String> getFormats() {
        return formats;
    }
}