package de.fhg.fokus.edp.mqa.service.persistence.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by bdi on 06/08/15.
 */
@Converter(autoApply = true)
public class LocalDateTimePersistenceConverter implements AttributeConverter<Object, Object> {
    @Override
    public Object convertToDatabaseColumn(Object o) {
        return Timestamp.valueOf((LocalDateTime) o);
    }

    @Override
    public Object convertToEntityAttribute(Object o) {
        return ((Timestamp) o).toLocalDateTime();
    }
}
