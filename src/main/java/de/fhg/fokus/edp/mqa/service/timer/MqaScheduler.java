package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.cdi.CdiJobFactory;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.duplicates.FingerprintScheduleParser;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static de.fhg.fokus.edp.mqa.service.config.Constants.*;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;


/**
 * Created by bdi on 06/05/15.
 */
@ApplicationScoped
@Named
public class MqaScheduler implements Serializable {

    /**
     * The Cdi job factory.
     */
    @Inject
    CdiJobFactory cdiJobFactory;

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.TIMER_VALIDATION_INTERVAL)
    private String validationCronSchedule;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.TIMER_SIMILARITY_INTERVAL)
    private String datasetSimilarityCronSchedule;

    @Inject
    private MqaTriggerListener mqaTriggerListener;

    @Inject
    private FingerprintScheduleParser fingerprintScheduleParser;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    private Scheduler scheduler;

    /**
     * Stores all currently fingerprinting languages at a central place. Is read from and written to from FingerprintService.
     * Tracking and ensuring no identical languages fingerprinting at one time is important so the respective files are only accessed by one process.
     */
    private HashSet<String> runningLangCodes;

    /**
     * CDI constructor
     * <p>
     * Initiates the timer for validation jobs. Starts automatically with the startup of the application server or right after a
     * deploy of the war archive.
     *
     * @param init the init
     */
    public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {

        runningLangCodes = new HashSet<>();

//        List<Catalog> allCatalogs = vdc.listAllCatalogs();
//        if (lastValidationRun == null && (allCatalogs != null && allCatalogs.size() > 0)) {
//            lastValidationRun = allCatalogs.stream().min(Comparator.comparing(Catalog::getDbUpdate)).get().getDbUpdate();
//        }

        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.setJobFactory(cdiJobFactory);
            scheduler.getListenerManager().addTriggerListener(mqaTriggerListener);
            scheduler.start();
        } catch (
                SchedulerException e) {
            LOG.error("Error getting scheduler started", e);
        }

        JobDetail validationJob = newJob(ValidationJob.class)
                .withIdentity(SCHEDULER_VALIDATION_JOB, SCHEDULER_VALIDATION_GROUP)
                .build();
        Trigger validationTrigger = newTrigger()
                .withIdentity(SCHEDULER_VALIDATION_TRIGGER, SCHEDULER_VALIDATION_GROUP)
                .withSchedule(cronSchedule(validationCronSchedule).withMisfireHandlingInstructionDoNothing())
                .forJob(validationJob)
                .build();

        JobDetail deletionJob = newJob(DeletionJob.class)
                .withIdentity(SCHEDULER_DELETION_JOB, SCHEDULER_DELETION_GROUP)
                .build();

        JobDetail datasetSimilarityJob = newJob(DatasetSimilarityJob.class)
                .withIdentity(SCHEDULER_DATASET_SIMILARITY_JOB, SCHEDULER_DATASET_SIMILARITY_GROUP)
                .build();
        Trigger datasetSimilarityTrigger = newTrigger()
                .withIdentity(SCHEDULER_DATASET_SIMILARITY_TRIGGER, SCHEDULER_DATASET_SIMILARITY_GROUP)
                .withSchedule(cronSchedule(datasetSimilarityCronSchedule).withMisfireHandlingInstructionDoNothing())
                .forJob(datasetSimilarityJob)
                .build();

        JobDetail reportJob = newJob(ReportGenerationJob.class)
                .withIdentity(SCHEDULER_REPORT_JOB, SCHEDULER_REPORT_GROUP)
                .build();
        Trigger reportTrigger = newTrigger()
                .withIdentity(SCHEDULER_REPORT_TRIGGER, SCHEDULER_REPORT_GROUP)
                .forJob(reportJob)
                .startNow()
                .build();

        try {
//            //FIXME doesn't work, never fired
//            JobListener validationJobListener = new JobListenerSupport() {
//                @Override
//                public String getName() {
//                    return "validationJobListener";
//                }
//
//                @Override
//                public void jobWasExecuted(JobExecutionContext context, JobExecutionException exception) {
//                    try {
//                        LOG.info("Deletion job triggered successfully after validation run");
//                        scheduler.triggerJob(deletionJob.getKey());
//                    } catch (SchedulerException e) {
//                        LOG.error("Failed to trigger deletion run after validation run");
//                        e.printStackTrace();
//                    }
//                }
//            };

//            scheduler.getListenerManager().addJobListener(validationJobListener, KeyMatcher.keyEquals(jobKey(Constants.SCHEDULER_VALIDATION_JOB, Constants.SCHEDULER_VALIDATION_GROUP)));
            scheduler.scheduleJob(validationJob, validationTrigger);
        } catch (
                SchedulerException e) {
            LOG.error("Error starting validation job", e);
        }

        try {
            scheduler.scheduleJob(datasetSimilarityJob, datasetSimilarityTrigger);
        } catch (
                SchedulerException e) {
            LOG.error("Error starting datasetSimilarity job", e);
        }

        try {
            scheduler.scheduleJob(reportJob, reportTrigger);
        } catch (
                SchedulerException e) {
            LOG.error("Error starting report job", e);
        }

        fingerprintScheduleParser.getFingerprintLanguagesWithSchedules().

                forEach(schedule ->

                {
                    try {
                        JobDetail job = newJob(FingerprintJob.class)
                                .withIdentity(SCHEDULER_FINGERPRINTING_JOB + schedule.getLanguages(), SCHEDULER_FINGERPRINTING_GROUP)
                                .usingJobData(JOB_LANG_KEY, schedule.getLanguages())
                                .usingJobData(JOB_RUN_ONLY_ONCE, false)
                                .build();

                        Trigger trigger = newTrigger()
                                .withIdentity(SCHEDULER_FINGERPRINTING_TRIGGER + schedule.getLanguages(), SCHEDULER_FINGERPRINTING_GROUP)
                                .withSchedule(cronSchedule(schedule.getInterval()))
                                .forJob(job)
                                .build();

                        scheduler.scheduleJob(job, trigger);
                        LOG.debug("Successfully scheduled fingerprinting job for language [{}] with schedule [{}]", schedule.getLanguages(), schedule);
                    } catch (Exception e) {
                        LOG.error("Error starting fingerprinting job with identity [{}]", schedule.getLanguages(), e);
                    }
                });
    }

    public boolean runValidationJobNow() throws SchedulerException {
        if (!isValidationJobRunning()) {
            JobDetail validationJob = newJob(ValidationJob.class)
                    .withIdentity(SCHEDULER_VALIDATION_JOB_INSTANT, SCHEDULER_VALIDATION_GROUP)
                    .build();

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(SCHEDULER_VALIDATION_TRIGGER_INSTANT, SCHEDULER_VALIDATION_GROUP)
                    .forJob(validationJob)
                    .startNow()
                    .build();

            scheduler.scheduleJob(validationJob, trigger);
            return true;
        } else {
            LOG.info("Validation job requested but a job is already running");
            return false;
        }
    }

    public boolean runReportJobNow() throws SchedulerException {
        if (!isValidationJobRunning()) {
            JobDetail reportGenerationJob = newJob(ReportGenerationJob.class)
                    .withIdentity(SCHEDULER_REPORT_JOB_INSTANT, SCHEDULER_REPORT_GROUP)
                    .build();

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(SCHEDULER_REPORT_TRIGGER_INSTANT, SCHEDULER_REPORT_GROUP)
                    .forJob(reportGenerationJob)
                    .startNow()
                    .build();

            scheduler.scheduleJob(reportGenerationJob, trigger);
            return true;
        } else {
            LOG.info("Validation job requested but a job is already running");
            return false;
        }
    }

    public boolean runDeletionJobNow() throws SchedulerException {
        if (!isValidationJobRunning() && !isDeletionJobRunning()) {
            JobDetail deletionJob = newJob(DeletionJob.class)
                    .withIdentity(SCHEDULER_DELETION_JOB_INSTANT, SCHEDULER_DELETION_GROUP)
                    .build();

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(SCHEDULER_DELETION_TRIGGER_INSTANT, SCHEDULER_DELETION_GROUP)
                    .startNow()
                    .build();

            scheduler.scheduleJob(deletionJob, trigger);
            return true;
        } else {
            LOG.info("Deletion job requested but a deletion/validation job is already running");
            return false;
        }
    }

    public boolean runFingerprintingJobNow(String langCodes) throws SchedulerException {
        final JobKey jobKey = new JobKey(SCHEDULER_FINGERPRINTING_JOB + langCodes + "_APIRUN",
                SCHEDULER_FINGERPRINTING_GROUP);
        final TriggerKey triggerKey = new TriggerKey(SCHEDULER_FINGERPRINTING_TRIGGER + langCodes + "_APIRUN",
                SCHEDULER_FINGERPRINTING_GROUP);

        if (!scheduler.checkExists(jobKey) && !scheduler.checkExists(triggerKey)) {
            JobDetail job = newJob(FingerprintJob.class)
                    .withIdentity(jobKey)
                    .usingJobData(JOB_LANG_KEY, langCodes)
                    .build();

            SimpleTrigger trigger = (SimpleTrigger) newTrigger()
                    .withIdentity(triggerKey)
                    .forJob(job)
                    .startNow()
                    .build();

            scheduler.scheduleJob(job, trigger);
            return true;
        } else {
            LOG.info("Fingerprinting job for language [{}] requested but a job is already running", langCodes);
            return false;
        }
    }

    public void rescheduleLanguageFingerprinting(String langCodes) {
        // generate unique filename for later removal
        final String identity = langCodes + "_RERUN";

        JobDetail job = newJob(FingerprintJob.class)
                .withIdentity(SCHEDULER_FINGERPRINTING_JOB + identity, SCHEDULER_FINGERPRINTING_GROUP)
                .usingJobData(JOB_LANG_KEY, langCodes)
                .build();

        SimpleTrigger simpleTrigger = (SimpleTrigger) newTrigger()
                .withIdentity(SCHEDULER_FINGERPRINTING_TRIGGER + identity, SCHEDULER_FINGERPRINTING_GROUP)
                .startAt(DateBuilder.futureDate(LANGUAGE_RETRY_WAIT_TIME_IN_HOURS, DateBuilder.IntervalUnit.HOUR))
                .forJob(job)
                .build();

        try {
            scheduler.scheduleJob(job, simpleTrigger);
            LOG.info("Rescheduled fingerprinting job for language [{}] at [{}]", langCodes, simpleTrigger.getNextFireTime());
        } catch (SchedulerException e) {
            LOG.error("Failed to reschedule fingerprinting job for language [{}] at [{}]", langCodes, simpleTrigger.getNextFireTime(), e);
        }
    }

    public void scheduleDeletionJobInFuture(int minutesFromNow) throws SchedulerException {
        JobDetail deletionJob = newJob(DeletionJob.class)
                .withIdentity(SCHEDULER_DELETION_JOB, SCHEDULER_DELETION_GROUP)
                .build();

        SimpleTrigger trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
                .withIdentity(SCHEDULER_DELETION_TRIGGER, SCHEDULER_DELETION_GROUP)
                .startAt(DateBuilder.futureDate(minutesFromNow, DateBuilder.IntervalUnit.MINUTE))
                .build();

        scheduler.scheduleJob(deletionJob, trigger);
    }

    public boolean isValidationJobRunning() throws SchedulerException {
        return scheduler.getCurrentlyExecutingJobs().stream().anyMatch(jobContext ->
                jobContext.getJobDetail().getKey().getGroup().equals(SCHEDULER_VALIDATION_GROUP));
    }

    public boolean isDeletionJobRunning() throws SchedulerException {
        return scheduler.getCurrentlyExecutingJobs().stream().anyMatch(jobContext ->
                jobContext.getJobDetail().getKey().getGroup().equals(SCHEDULER_DELETION_GROUP));
    }

    public LocalDateTime nextValidationRun() throws SchedulerException {
        return LocalDateTime.ofInstant(getAllSchedulerTriggers().stream().filter(trigger
                -> StringUtils.equals(trigger.getJobKey().getName(), SCHEDULER_VALIDATION_JOB)).findFirst().get().getNextFireTime().toInstant(), ZoneId.systemDefault());
    }

    private List<Trigger> getAllSchedulerTriggers() throws SchedulerException {
        return scheduler.getTriggerKeys(GroupMatcher.anyGroup()).stream().map(this::safeGetTrigger).collect(Collectors.toList());
    }

    private Trigger safeGetTrigger(TriggerKey triggerKey) {
        try {
            return scheduler.getTrigger(triggerKey);
        } catch (SchedulerException e) {
            LOG.error("Could not retrieve trigger key: {}", e);
        }

        return null;
    }

    private Trigger getValidationTrigger() throws SchedulerException {
        List<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(SCHEDULER_VALIDATION_GROUP)).stream().filter(jobKey ->
                StringUtils.equals(jobKey.getName(), SCHEDULER_VALIDATION_JOB)).collect(Collectors.toList());

        List<Trigger> triggers = new ArrayList<>();

        for (JobKey jobKey : jobKeys) {
            triggers.addAll(scheduler.getTriggersOfJob(jobKey));
        }

        return triggers.stream().max(Comparator.comparing(Trigger::getStartTime)).get();
    }

    private List<Trigger> safeGetTriggersOfJobKey(JobKey jobKey) {
        List<Trigger> trigger = new ArrayList<>();
        try {
            trigger.addAll(scheduler.getTriggersOfJob(jobKey));
        } catch (SchedulerException e) {
            LOG.error("Error receiving triggers of job key {}: {}", jobKey, e.getStackTrace());
        }
        return trigger;
    }


    public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init) {
        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            LOG.error("Cannot shutdown scheduler", e);
        }
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public HashSet<String> getRunningLangCodes() {
        return runningLangCodes;
    }

    public LocalDateTime getPreviousValidationRunDate() {
        try {
            return LocalDateTime.ofInstant(getValidationTrigger().getPreviousFireTime().toInstant(), ZoneId.systemDefault());
        } catch (SchedulerException | NullPointerException e) {
            LOG.warn("No previous validation run determined, returning null");
            return null;
        }
    }

    public LocalDateTime getPreviousOrNextValidationRunDate() {
        try {

            Date date = getValidationTrigger().getPreviousFireTime();
            if (date == null) {
                date = getValidationTrigger().getStartTime();
            }
            if (date == null) {
                date = getValidationTrigger().getNextFireTime();
            }

            return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        } catch (SchedulerException e) {
            LOG.warn("No previous validation run determined, returning null");
            return null;
        }
    }
}
