package de.fhg.fokus.edp.mqa.service.namedBeans;

import java.io.Serializable;

/**
 * Created by bdi on 29/05/15.
 */
public class ReportElementBean implements Serializable, Comparable<ReportElementBean> {

    private String label;
    private long value;

    /**
     * Creates a new PieChart object that can be used for chart rendering.
     *
     * @param value The numeric value of the corresponding label.
     * @param label The label that describes the value, its belongings to.
     */
    public ReportElementBean(long value, String label) {
        this.label = label;
        this.value = value;
    }

    /**
     *
     * Compares the values and if the values are equal it compares the label.
     *
     * @param o The object to compare with
     * @return The sort order of the compared objects
     */
    @Override
    public int compareTo(ReportElementBean other) {
        if (this.getValue() != other.getValue())
            return Math.toIntExact(other.getValue() - this.getValue());

        return this.getLabel().compareTo(other.getLabel());
    }

    /**
     * Gets label.
     *
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets label.
     *
     * @param label the label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public long getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(long value) {
        this.value = value;
    }
}
