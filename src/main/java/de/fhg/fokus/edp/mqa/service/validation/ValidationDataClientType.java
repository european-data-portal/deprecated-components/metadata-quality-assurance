package de.fhg.fokus.edp.mqa.service.validation;

/**
 * Created by bdi on 10/04/15.
 */
public enum ValidationDataClientType {
    /**
     * Persistence validation data client type.
     */
    PERSISTENCE
}
