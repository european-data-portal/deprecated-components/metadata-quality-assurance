package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by bdi on 25/01/16.
 */
@RequestScoped
@Named
public class CatalogDashboardDistributionBean extends CatalogDiagramBean implements Serializable {

    private Set<ReportElementBean> availableDistributionsPercentagePieChart;
    private Set<ReportElementBean> errorCodesPieChart;
    private Set<ReportElementBean> withDownloadUrlPieChart;
    private Set<ReportElementBean> machineReadableDatasetPieChart;
    private Set<ReportElementBean> mostUsedFormats;

    @Override
    public boolean renderSection() {
        return getRenderMetric("/metric/catalogues/" + catalogId + "/render/distributions");
    }

    @Override
    void createCharts() {
        availableDistributionsPercentagePieChart = getSetWithDynamicKeysAndMultipleValues("/metric/catalogues/" + catalogId + "/distributions/accessibility");
        errorCodesPieChart = getSetWithFixedKeysAndMultipleValues("/metric/catalogues/" + catalogId + "/distributions/status_codes");
        withDownloadUrlPieChart = getSetWithBinaryValues("/metric/catalogues/" + catalogId + "/distributions/download_url_exists");
        machineReadableDatasetPieChart = getSetWithBinaryValues("/metric/catalogues/" + catalogId + "/distributions/machine_readable");
        mostUsedFormats = getSetWithMultipleValuesSorted("/metric/catalogues/" + catalogId + "/distributions/formats", 10);
    }

    /**
     * Gets available distributions percentage pie chart.
     *
     * @return the available distributions percentage pie chart
     */
    public Set<ReportElementBean> getAvailableDistributionsPercentagePieChart() {
        return availableDistributionsPercentagePieChart;
    }

    /**
     * Sets available distributions percentage pie chart.
     *
     * @param availableDistributionsPercentagePieChart the available distributions percentage pie chart
     */
    public void setAvailableDistributionsPercentagePieChart(Set<ReportElementBean> availableDistributionsPercentagePieChart) {
        this.availableDistributionsPercentagePieChart = availableDistributionsPercentagePieChart;
    }

    /**
     * Gets error codes pie chart.
     *
     * @return the error codes pie chart
     */
    public Set<ReportElementBean> getErrorCodesPieChart() {
        return errorCodesPieChart;
    }

    /**
     * Sets error codes pie chart.
     *
     * @param errorCodesPieChart the error codes pie chart
     */
    public void setErrorCodesPieChart(Set<ReportElementBean> errorCodesPieChart) {
        this.errorCodesPieChart = errorCodesPieChart;
    }

    /**
     * Gets with download url pie chart.
     *
     * @return the with download url pie chart
     */
    public Set<ReportElementBean> getWithDownloadUrlPieChart() {
        return withDownloadUrlPieChart;
    }

    /**
     * Sets with download url pie chart.
     *
     * @param withDownloadUrlPieChart the with download url pie chart
     */
    public void setWithDownloadUrlPieChart(Set<ReportElementBean> withDownloadUrlPieChart) {
        this.withDownloadUrlPieChart = withDownloadUrlPieChart;
    }

    /**
     * Gets machine readable distributions pie chart.
     *
     * @return the machine readable distributions pie chart
     */
    public Set<ReportElementBean> getMachineReadableDatasetPieChart() {
        return machineReadableDatasetPieChart;
    }

    /**
     * Sets machine readable distributions pie chart.
     *
     * @param machineReadableDatasetPieChart the machine readable distributions pie chart
     */
    public void setMachineReadableDatasetPieChart(Set<ReportElementBean> machineReadableDatasetPieChart) {
        this.machineReadableDatasetPieChart = machineReadableDatasetPieChart;
    }

    /**
     * Gets most used formats.
     *
     * @return the most used formats
     */
    public Set<ReportElementBean> getMostUsedFormats() {
        return mostUsedFormats;
    }

    /**
     * Sets most used formats.
     *
     * @param mostUsedFormats the most used formats
     */
    public void setMostUsedFormats(Set<ReportElementBean> mostUsedFormats) {
        this.mostUsedFormats = mostUsedFormats;
    }
}
