package de.fhg.fokus.edp.mqa.service.duplicates;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by fritz on 15.02.17.
 */
@ApplicationScoped
public class FingerprintScheduleParser {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.FP_SCHEDULE_PATH)
    private String fingerprintSchedulePath;

    public ArrayList<Schedule> getFingerprintLanguagesWithSchedules() {
        ArrayList<Schedule> fingerprintSchedules = new ArrayList<>();

        try {
            JAXBContext context = JAXBContext.newInstance(Schedules.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            // parse xml
            Schedules configSchedules = (Schedules) unmarshaller.unmarshal(new File(fingerprintSchedulePath));
            fingerprintSchedules = configSchedules.getSchedules();
        } catch (Exception e) {
            LOG.warn("Failed to read scheduling info from [{}]", fingerprintSchedulePath);
        }

        return fingerprintSchedules;
    }

    public HashSet<String> parseLanguageCodes(String langCodes) {
        HashSet<String> languages = new HashSet<>();

        if (!langCodes.isEmpty()) {

            // resolve wildcard
            if (Constants.LANGUAGE_WILDCARD.equals(langCodes)) {
                languages = new HashSet<>(Arrays.asList(allLanguageCodes));
            } else {
                // add all language codes with length 2, assuming those are the valid ones
                for (String code : langCodes.split(Constants.LANG_CODE_DELIMITER)) {
                    if (code.length() == 2) {
                        languages.add(code.toUpperCase());
                    } else {
                        LOG.warn("Ignoring invalid language code [{}]", code);
                    }
                }
            }
        } else {
            LOG.error("No languages configured");
        }

        return languages;
    }

    public static class Schedule {
        private String languages;
        private String interval;

        public String getLanguages() {
            return languages;
        }

        public void setLanguages(String languages) {
            this.languages = languages;
        }

        public String getInterval() {
            return interval;
        }

        public void setInterval(String interval) {
            this.interval = interval;
        }
    }

    @XmlRootElement(name = "schedules")
    public static class Schedules {
        private ArrayList<Schedule> schedules;

        @XmlElement(name = "schedule")
        public ArrayList<Schedule> getSchedules() {
            return schedules;
        }

        public void setSchedules(ArrayList<Schedule> schedules) {
            this.schedules = schedules;
        }
    }

    // stores all possible language codes to allow use of an asterisk in config
    private static final String[] allLanguageCodes = {
            "AT", "BE", "BG", "CH", "CY",
            "CZ", "DE", "DK", "EE", "ES",
            "FI", "FR", "GB", "GR", "HR",
            "HU", "IE", "IS", "IT", "LI",
            "LT", "LU", "LV", "MD", "MT",
            "NL", "NO", "PL", "PT", "RO",
            "RS", "SE", "SI", "SK", "__"
    };
}
