package de.fhg.fokus.edp.mqa.service.report;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.namedBeans.diagrams.*;
import de.fhg.fokus.edp.mqa.service.timer.MqaScheduler;
import de.fhg.fokus.edp.mqa.service.utils.LocaleUtil;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static de.fhg.fokus.edp.mqa.service.config.Constants.MSG_BUNDLE;

/**
 * Created by fritz on 16.11.16.
 */
public abstract class ReportGenerator {

    @Inject
    @Log
    Logger LOG;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.REPORT_DIRECTORY)
    String reportDirectory;

    @Inject
    private MqaScheduler mqaScheduler;

    @Inject
    DashboardDistributionsDiagramsBean dashboardDistributionsDiagramsBean;

    @Inject
    DashboardViolationsDiagramsBean dashboardViolationsDiagramsBean;

    @Inject
    DashboardLicencesDiagramsBean dashboardLicencesDiagramsBean;

    @Inject
    CatalogDashboardDistributionBean catalogDashboardDistributionBean;

    @Inject
    CatalogDashboardViolationBean catalogDashboardViolationBean;

    @Inject
    CatalogDashboardLicenceBean catalogDashboardLicenceBean;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    private LocaleUtil localeUtil;
    ResourceBundle rb;

    File reportFile;

    String lastRun, currentDate;

    List<Catalog> catalogs;

    String  dashboardTitle,
            dashboardOverview,
            dashboardLastUpdate,
            dashboardCurrentDate,
            distAccessibilityAll,
            distStatusCodes,
            distWithoutDownloadUrl,
            distAccessibilityCatalog,
            distMachineReadablePercentage,
            distFormatMostUsed,
            distMachineReadableCount,
            violationMostOccurred,
            violationComplianceAll,
            violationComplianceCatalog,
            licenceKnownPercentage,
            licenceMostUsed,
            licenceCatalogsWithMostKnown;

    boolean renderGlobalDistributions, renderGlobalViolations, renderGlobalLicences;


    @PostConstruct
    public void init() {
        // get list of catalogs
        catalogs = vdc.listAllCatalogs();
        renderGlobalDistributions = vdc.countAllDistributions() > 0;
        renderGlobalViolations = vdc.countAllViolations() > 0;
        renderGlobalLicences = vdc.countAllLicences() > 0;
    }

    void reloadLanguageSpecificData() {

        Locale locale = localeUtil.getCurrentLocale();
        rb = ResourceBundle.getBundle(MSG_BUNDLE, locale);

        // reload diagrams to set labels according to current locale
        dashboardDistributionsDiagramsBean.loadDiagramsWithLocale(locale);
        dashboardViolationsDiagramsBean.loadDiagramsWithLocale(locale);
        dashboardLicencesDiagramsBean.loadDiagramsWithLocale(locale);

        // set label texts
        try {
            dashboardTitle = rb.getString("report.title");
            dashboardOverview = rb.getString("report.dashboard.header");
            dashboardLastUpdate = rb.getString("hf.last_update");
            dashboardCurrentDate = rb.getString("report.date.created");
            distAccessibilityAll = rb.getString("dashboard.distribution.overall_accessibility");
            distStatusCodes = rb.getString("dashboard.distribution.status_codes");
            distWithoutDownloadUrl = rb.getString("dashboard.distribution.with_download_url");
            distAccessibilityCatalog = rb.getString("dashboard.distribution.accessibility_per_catalog");
            distMachineReadablePercentage = rb.getString("dashboard.distribution.format.machinereadable.percentage");
            distFormatMostUsed = rb.getString("dashboard.distribution.format.mostused");
            distMachineReadableCount = rb.getString("dashboard.distribution.format.machinereadable.catalog.count.title");
            violationMostOccurred = rb.getString("dashboard.compliance.violation_places");
            violationComplianceAll = rb.getString("dashboard.compliance.dataset_in_percentage");
            violationComplianceCatalog = rb.getString("dashboard.compliance.non_compliant_dataset_per_catalog_in_percentage");
            licenceKnownPercentage = rb.getString("dashboard.licence.known.percentage");
            licenceMostUsed = rb.getString("dashboard.licence.mostused");
            licenceCatalogsWithMostKnown = rb.getString("dashboard.licence.catalogue.known.subtitle");
        } catch (MissingResourceException e) {
            LOG.warn(e.getMessage());
        }

        // set current date and time of last run
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                .withLocale(localeUtil.getCurrentLocale());

        currentDate = LocalDateTime.now().format(formatter);

        LocalDateTime lastRunTime = mqaScheduler.getPreviousOrNextValidationRunDate();

        if (lastRunTime != null) {
            lastRun = mqaScheduler.getPreviousOrNextValidationRunDate().format(formatter);
        } else {
            lastRun = rb.getString("common.error");
            LOG.error("Last run time has not been set in scheduler.");
        }
    }

    void reloadCatalogDiagrams(String catalogId) {
        Locale locale = localeUtil.getCurrentLocale();
        catalogDashboardDistributionBean.fillWithSpecificCatalog(catalogId, locale);
        catalogDashboardViolationBean.fillWithSpecificCatalog(catalogId, locale);
        catalogDashboardLicenceBean.fillWithSpecificCatalog(catalogId, locale);
    }

    void setReportFile(ReportHandler.ReportFormat format, String language) throws IOException {
        Path path = Paths.get(reportDirectory, String.format("mqa-report_%s.%s", language, format.name().toLowerCase()));

        Files.deleteIfExists(path);
        Path filePath = Files.createFile(path);
        reportFile = filePath.toFile();
    }

    abstract void generateReport(String language);
}
