package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.fetch.MqaHttpProvider;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;
import de.fhg.fokus.edp.mqa.service.utils.LocaleUtil;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.Serializable;
import java.util.*;

/**
 * Created by fritz on 03.11.16.
 */
abstract class DiagramBean implements Serializable {

    @Inject
    @Log
    Logger LOG;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    ValidationDataClient vdc;

    @Inject
    private MqaHttpProvider httpProvider;

    @Inject
    private LocaleUtil localeUtil;

    private ResourceBundle rb;

    // used by extending class to toggle loading of diagrams
    boolean isSaveToLoadDiagrams = true;

    @PostConstruct
    public void init() {
        // load initial locale
        Locale locale;
        try {
            locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        } catch (NullPointerException ex) {
            locale = localeUtil.getCurrentLocale();
        }

        if (isSaveToLoadDiagrams)
            loadDiagramsWithLocale(locale);
    }

    // sets locale and recreates diagrams to ensure correct labeling
    public void loadDiagramsWithLocale(Locale locale) {
        rb = ResourceBundle.getBundle(Constants.MSG_BUNDLE, locale);
        createCharts();
    }

    public abstract boolean renderSection();

    // recreate charts
    abstract void createCharts();

    boolean getRenderMetric(String endpoint) {
        JsonObject metric = httpProvider.getMetric(endpoint).orElseGet(this::getDefaultRenderResult);

        if (!metric.getBoolean("success"))
            metric = getDefaultRenderResult();

        return metric.getBoolean("result");
    }

    // intended for yes/no percentage pie charts
    TreeSet<ReportElementBean> getSetWithBinaryValues(String endpoint) {
        JsonObject metric = httpProvider.getMetric(endpoint).orElseGet(this::getDefaultBinaryPercentages);

        if (!metric.getBoolean("success"))
            metric = getDefaultBinaryPercentages();

        TreeSet<ReportElementBean> result = new TreeSet<>();
        result.add(new ReportElementBean(Math.round(metric.getJsonObject("result").getJsonNumber("yes").doubleValue()), rb.getString("common.yes")));
        result.add(new ReportElementBean(Math.round(metric.getJsonObject("result").getJsonNumber("no").doubleValue()), rb.getString("common.no")));

        return result;
    }

    // intended for pie charts with multiple values, no ordering or limiting of size
    TreeSet<ReportElementBean> getSetWithFixedKeysAndMultipleValues(String endpoint) {
        JsonObject metric = httpProvider.getMetric(endpoint).orElseGet(this::getDefaultListWithFixedKeysPercentages);

        if (!metric.getBoolean("success"))
            metric = getDefaultListWithFixedKeysPercentages();

        TreeSet<ReportElementBean> result = new TreeSet<>();
        metric.getJsonArray("result").forEach(entry ->
                result.add(new ReportElementBean(
                        Math.round(((JsonObject) entry).getJsonNumber("percentage").doubleValue()),
                        ((JsonObject) entry).getString("name").replaceAll("^\"|\"$", "")))); // remove quotes from start & end

        return result;
    }

    // intended for pie charts with multiple values, no ordering or limiting of size
    TreeSet<ReportElementBean> getSetWithDynamicKeysAndMultipleValues(String endpoint) {
        JsonObject metric = httpProvider.getMetric(endpoint).orElseGet(this::getDefaultListWithDynamicKeysPercentages);


        if (!metric.getBoolean("success"))
            metric = getDefaultListWithDynamicKeysPercentages();

        TreeSet<ReportElementBean> result = new TreeSet<>();

        JsonObject jsonResult = metric.getJsonObject("result");
        jsonResult.keySet().forEach(key ->
                result.add(new ReportElementBean(Math.round(jsonResult.getJsonNumber(key).doubleValue()), key)));

        return result;
    }

    // intended for sorted bar charts, applies a size limit
    TreeSet<ReportElementBean> getSetWithMultipleValuesSorted(String endpoint, int limit) {
        JsonObject metric = httpProvider.getMetric(endpoint).orElseGet(this::getDefaultListWithFixedKeysPercentages);

        if (!metric.getBoolean("success"))
            metric = getDefaultListWithFixedKeysPercentages();

        List<ReportElementBean> result = new ArrayList<>();
        metric.getJsonArray("result").forEach((entry) ->
                result.add(new ReportElementBean(
                        Math.round(((JsonObject) entry).getJsonNumber("percentage").doubleValue()),
                        ((JsonObject) entry).getString("name").replaceAll("^\"|\"$", "")))); // remove quotes from start & end

        result.sort((r1, r2) -> {
            if (r1.getValue() != r2.getValue())
                return Math.toIntExact(r2.getValue() - r1.getValue());

            return r1.getLabel().compareTo(r2.getLabel());
        });

        return new TreeSet<>(result.subList(0, limit < result.size() ? limit : result.size()));
    }

    private JsonObject getDefaultRenderResult() {
        return Json.createObjectBuilder()
                .add("success", true)
                .add("result", false)
                .build();
    }

    // return empty array
    private JsonObject getDefaultListWithFixedKeysPercentages() {
        return Json.createObjectBuilder()
                .add("success", true)
                .add("result", Json.createArrayBuilder().build())
                .build();
    }

    // return empty object
    private JsonObject getDefaultListWithDynamicKeysPercentages() {
        return Json.createObjectBuilder()
                .add("success", true)
                .add("result", Json.createObjectBuilder().build())
                .build();
    }

    // return 100 % 'no'
    private JsonObject getDefaultBinaryPercentages() {
        return Json.createObjectBuilder()
                .add("success", true)
                .add("result", Json.createObjectBuilder()
                        .add("yes", 0.0)
                        .add("no", 100.0)
                        .build())
                .build();
    }
}
