package de.fhg.fokus.edp.mqa.service.utils;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Locale;

/**
 * Created by bdi on 22/04/16.
 */
@SessionScoped
public class LocaleUtil implements Serializable {

    private Locale currentLocale;

    /**
     * Gets current locale.
     *
     * @return the current locale
     */
    public Locale getCurrentLocale() {
        return currentLocale;
    }

    /**
     * Sets current locale.
     *
     * @param currentLocale the current locale
     */
    public void setCurrentLocale(Locale currentLocale) {
        this.currentLocale = currentLocale;
    }
}
