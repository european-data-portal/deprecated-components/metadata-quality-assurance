package de.fhg.fokus.edp.mqa.service.validation;

import de.fhg.fokus.edp.mqa.service.model.*;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by bdi on 10/04/15.
 */
public interface ValidationDataClient {

    /**
     * Gets all datasets.
     *
     * @param firstRow the first row
     * @param rowCount the row count
     * @return the all datasets
     */
    List<Dataset> listAllDatasets(int firstRow, int rowCount);

    /**
     * Gets dataset.
     *
     * @param name the name
     * @return the dataset
     * @throws NoResultException the no result exception
     */
    Dataset getDatasetByName(String name) throws NoResultException;

    /**
     * Dataset list not ok by catalog list.
     *
     * @param firstRow    the first row
     * @param rowCount    the row count
     * @param catalogName the catalog name
     * @return the list
     */
    List<Dataset> listDatasetsWithStatusCodesNotOkByCatalogName(int firstRow, int rowCount, String catalogName);

    /**
     * Dataset list with violations by catalog list.
     *
     * @param firstRow    the first row
     * @param rowCount    the row count
     * @param catalogName the catalog name
     * @return the list
     */
    List<Dataset> listDatasetsNonConformByCatalogName(int firstRow, int rowCount, String catalogName);

    /**
     * Dataset count int.
     *
     * @return the int
     */
    int countAllDatasets();

    /**
     * Dataset conform count int.
     *
     * @param conform the conform
     * @return the int
     */
    int countDatasetsConform(boolean conform);

    /**
     * Dataset list distribution not ok by catalog name count int.
     *
     * @param catalogName the catalog name
     * @return the int
     */
    int countDatasetsWithStatusCodesNotOkByCatalogName(String catalogName);

    /**
     * Dataset list with violations by catalog count int.
     *
     * @param catalogName the catalog name
     * @return the int
     */
    int countDatasetsNonConformByCatalogName(String catalogName);

    /**
     * Is distribution existing boolean.
     *
     * @param instanceId the instance id
     * @return the boolean
     */
    boolean isDistributionExistingById(String instanceId);

    /**
     * Gets catalog.
     *
     * @param name the name
     * @return the catalog
     */
    Catalog getCatalogByName(String name);

    /**
     * Gets catalog, but without its datasets (lazy)
     *
     * @param name the name
     * @return the catalog
     */
    Catalog getCatalogWithoutDatasetsByName(String name);

    /**
     * Gets catalog names.
     *
     * @return the catalog names
     */
    List<String> listAllCatalogNames();

    /**
     * Find dataset names by catalog name list.
     *
     * @param catalogName the catalog name
     * @return the list
     */
    List listDatasetNamesByCatalogName(String catalogName);

    /**
     * Create dataset dataset.
     *
     * @return the dataset
     */
    Dataset createDataset();

    /**
     * Create catalog catalog.
     *
     * @return the catalog
     */
    Catalog createCatalog();

    /**
     * Create distribution distribution.
     *
     * @return the distribution
     */
    Distribution createDistribution();

    /**
     * Create dataset similarity dataset similarity.
     *
     * @return the dataset similarity
     */
    DatasetSimilarity createDatasetSimilarity();

    /**
     * Create violation de . fhg . fokus . mqa . mqa . service . model . violation.
     *
     * @return the de . fhg . fokus . mqa . mqa . service . model . violation
     */
    Violation createViolation();

    /**
     * Create licence de . fhg . fokus . mqa . mqa . service . model . licence.
     *
     * @return the de . fhg . fokus . mqa . mqa . service . model . licence
     */
    Licence createLicence();

    /**
     * Gets all distributions.
     *
     * @param firstRow the first row
     * @param rowCount the row count
     * @return the all distributions
     */
    List listAllDistributions(int firstRow, int rowCount);

    /**
     * Distribution find distribution.
     *
     * @param distributionInstanceId the distribution id
     * @return the distribution
     */
    Distribution getDistributionByInstanceId(String distributionInstanceId);

    /**
     * Distributions conform and ok int.
     *
     * @return the int
     */
    int countDistributionsWithStatusCodesOk();

    /**
     * Distribution status codes set.
     *
     * @return the set
     */
    Set<Integer> listDistributionStatusCodes();

    /**
     * Distribution status code count by status code int.
     *
     * @param statusCode the status code
     * @return the int
     */
    int countDistributionStatusCodesByStatusCode(int statusCode);

    /**
     * Distribution download url count int.
     *
     * @return the int
     */
    int countDistributionsWithDownloadUrl();

    /**
     * Distribution url ok count int.
     *
     * @param accessUrl the access url
     * @return the int
     */
    int countDistributionsWithUrlOkByUrlType(boolean accessUrl);

    /**
     * Distribution get all count int.
     *
     * @return the int
     */
    int countAllDistributions();

    /**
     * Distribution count by catalog int.
     *
     * @param catalogName the catalog name
     * @return the int
     */
    int countDistributionsByCatalogName(String catalogName);

    /**
     * Distribution find by dataset list.
     *
     * @param name the name
     * @return the list
     */
    List listDistributionsByDatasetName(String name);

    /**
     * Violation find by catalog list.
     *
     * @param name the name
     * @return the list
     */
    List listViolationsByCatalogName(String name);

    /**
     * Violation find by dataset list.
     *
     * @param name the name
     * @return the list
     */
    List listViolationsByDatasetName(String name);

    /**
     * Violation get all list.
     *
     * @return the list
     */
    List listAllViolations();

    /**
     * Violation get count long.
     *
     * @return the long
     */
    long countAllViolations();

    /**
     * Violation count for catalog long.
     *
     * @param catalog the catalog
     * @return the long
     */
    long countViolationsByCatalog(Catalog catalog);

    /**
     * Violation get names set.
     *
     * @return the set
     */
    Set<String> listViolationNames();

    /**
     * List catalogs names list.
     *
     * @param firstRow the first row
     * @param rowCount the row count
     * @return the list
     */
    List listAllCatalogNamesWithPaging(int firstRow, int rowCount);

    /**
     * Find all catalogs list.
     *
     * @return the list
     */
    List listAllCatalogs();

    long countAllCatalogs();

    /**
     * List catalogs list.
     *
     * @param firstRow the first row
     * @param rowCount the row count
     * @return the list
     */
    List listAllCatalogsWithPaging(int firstRow, int rowCount);

    /**
     * List datasets list.
     *
     * @param firstRow the first row
     * @param rowCount the row count
     * @return the list
     */
    List listAllDatasetNamesWithPaging(int firstRow, int rowCount);

    /**
     * List distributions list.
     *
     * @param firstRow the first row
     * @param rowCount the row count
     * @return the list
     */
    List listAllDistributionIdsWithPaging(int firstRow, int rowCount);

    /**
     * Gets a map with all catalog names and the number of non conformant datasets
     *
     * @return the catalog map with non conformant datasets count
     */
    Map<Catalog, Long> listAllCatalogsWithNonConformantDatasetCount();

    /**
     * Map catalogs with accessible distributions map.
     *
     * @param limit the limit
     * @return the map
     */
    Map<Catalog, Long> listCatalogsWithAccessibleDistributions();

    /**
     * Gets most occurred violation names with count.
     *
     * @param limit the limit
     * @return the most occurred violation names with count
     */
    Map<String, Long> listViolationMostCommonWithCount(int limit);

    /**
     * Gets most occurred violation names with count for catalog.
     *
     * @param catalog the catalog
     * @param limit   the limit
     * @return the most occurred violation names with count for catalog
     */
    Map<String, Long> listViolationsMostCommonWithCountByCatalog(Catalog catalog, int limit);

    /**
     * List most used distribution formats map.
     *
     * @param max the max
     * @return the map
     */
    Map<String, Long> listDistributionFormatsMostUsed(int max);

    /**
     * Gets machine readable distribution count by catalog.
     *
     * @param max the max
     * @return the machine readable distribution count by catalog
     */
    Map<Catalog, Long> listCatalogsWithMachineReadableDistributionsCount();

    Map<Catalog, Long> listCatalogsWithMachineReadableDatasetsCount();

    long countMachineReadableDatasetsByCatalog(Catalog catalog);

    long countMachineReadableDatasets();

    /**
     * Gets distributions count per catalog.
     *
     * @return the distributions count per catalog
     */
    Map<String, Long> listCatalogIdsWithDistributionCount();
    Map<String, Long> listCatalogIdsWithDatasetCount();

    /**
     * List most used distribution formats for catalog map.
     *
     * @param catalog the catalog
     * @param max     the max
     * @return the map
     */
    Map<String, Long> listDistributionFormatsMostUsedByCatalog(Catalog catalog, int max);

    /**
     * Gets count of available distributions of catalog.
     *
     * @param catalogInstanceId the catalog instance id
     * @return the count of available distributions of catalog
     */
    long countDistributionsAvailableByCatalogId(String catalogInstanceId);

    /**
     * Dataset similarity is existing boolean.
     *
     * @param datasetSimilarity the dataset similarity
     * @return the boolean
     */
    boolean isDatasetSimilarityExisting(DatasetSimilarity datasetSimilarity);

    /**
     * Machine readable distributions count long.
     *
     * @return the long
     */
    long countDistributionsMachineReadable();

    /**
     * Gets count of distributions with download url by catalog.
     *
     * @param catalog the catalog
     * @return the count of distributions with download url by catalog
     */
    long countDistributionsWithDownloadUrlByCatalog(Catalog catalog);

    /**
     * Gets machine readable distributions of catalog.
     *
     * @param catalog the catalog
     * @return the machine readable distributions of catalog
     */
    long countMachineReadableDistributionsByCatalog(Catalog catalog);

    /**
     * Gets distribution status codes of catalog.
     *
     * @param catalog the catalog
     * @return the distribution status codes of catalog
     */
    Map<Integer, Long> listDistributionStatusCodesWithCountByCatalog(Catalog catalog);

    /**
     * Count datasets for catalog long.
     *
     * @param catalog the catalog
     * @return the long
     */
    long countDatasetsByCatalog(Catalog catalog);

    /**
     * Count conform datasets for catalog long.
     *
     * @param catalog the catalog
     * @return the long
     */
    long countDatasetsConformByCatalog(Catalog catalog);

    /**
     * Gets licence.
     *
     * @param licenceId the id
     * @return the licence
     */
    Licence getLicenceById(String licenceId);

    /**
     * Licence get all count int.
     *
     * @return the int
     */
    int countAllLicences();

    /**
     * Get licence count for catalog long.
     *
     * @param catalog the catalog
     * @return the long
     */
    long countLicencesNonNullByCatalogName(String catalogName);

    /**
     * List most used licences map.
     *
     * @param max the max
     * @return the map
     */
    Map<String, Long> listMostUsedLicencesWithCount(int max);

    /**
     * List most used licences map.
     *
     * @param max the max
     * @param catalogName the catalog name
     * @return the map
     */
    Map<String, Long> listMostUsedLicencesWithCountByCatalogName(int max, String catalogName);

    /**
     * Datasets with known licences count long.
     *
     * @return the long
     */
    long countDatasetsWithKnownLicences();

    /**
     * Datasets with known licences count long.
     *
     * @param catalogName the catalog name
     * @return the long
     */
    long countDatasetsWithKnownLicencesByCatalogName(String catalogName);

    /**
     *  Catalogues datasets with known licence count map.
     *
     * @param max the max
     * @return the map
     */
    Map<Catalog, Long> listCatalogsWithMostDatasetsOfKnownLicenceWithCount();

    void removeOutdatedEntities(LocalDateTime date);

    /**
     * Persist entity t.
     *
     * @param <T>    the type parameter
     * @param entity the entity
     * @return the t
     */
    <T> T persistEntity(T entity);

    /**
     * Update entity t.
     *
     * @param <T>    the type parameter
     * @param entity the entity
     * @return the t
     */
    <T> T updateEntity(T entity);

    /**
     * Remove entity.
     *
     * @param <T>    the type parameter
     * @param entity the entity
     */
    <T> void removeEntity(T entity);

}
