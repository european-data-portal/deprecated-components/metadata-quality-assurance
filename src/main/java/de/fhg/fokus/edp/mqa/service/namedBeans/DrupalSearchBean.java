package de.fhg.fokus.edp.mqa.service.namedBeans;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by bdi on 03/11/15.
 */
@Named
@SessionScoped
public class DrupalSearchBean implements Serializable {

    @Inject
    private LanguageBean languageBean;

    private String drupalSearchString;

    /**
     * Search in drupal and redirect.
     *
     * @throws IOException the io exception
     */
    public void searchInDrupalAndRedirect() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(String.format("/%s/search/site/%s", languageBean.getLocaleCode(),
                drupalSearchString));
    }

    /**
     * Gets drupal search string.
     *
     * @return the drupal search string
     */
    public String getDrupalSearchString() {
        return drupalSearchString;
    }

    /**
     * Sets drupal search string.
     *
     * @param drupalSearchString the drupal search string
     */
    public void setDrupalSearchString(String drupalSearchString) {
        this.drupalSearchString = drupalSearchString;
    }
}
