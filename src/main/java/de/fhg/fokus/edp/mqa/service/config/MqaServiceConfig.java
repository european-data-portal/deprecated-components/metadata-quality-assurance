package de.fhg.fokus.edp.mqa.service.config;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by bdi on 09/04/15.
 */
@Qualifier
@Retention(RUNTIME)
@Target({TYPE, METHOD, FIELD, PARAMETER})
public @interface MqaServiceConfig {

    /**
     * Value string.
     *
     * @return the string
     */
    @Nonbinding String value();

    /**
     * Default value string.
     *
     * @return the string
     */
    @Nonbinding String defaultValue() default "";
}
