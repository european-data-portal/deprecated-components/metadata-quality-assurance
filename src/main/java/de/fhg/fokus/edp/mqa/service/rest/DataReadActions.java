package de.fhg.fokus.edp.mqa.service.rest;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.duplicates.SimilarityService;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.report.ReportHandler;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bdi on 25/05/16.
 */
@Path("/")
public class DataReadActions {

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private SimilarityService similarityService;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.REPORT_DIRECTORY)
    String reportDirectory;

    @GET
    @Path("catalog")
    @Produces("application/json")
    public Response getListOfCatalogs(@DefaultValue("1000") @QueryParam("rows") int rows,
                                      @DefaultValue("0") @QueryParam("start") int start) {
        List<String> catalogNames = vdc.listAllCatalogNamesWithPaging(start, rows);
        return Response.ok().entity(catalogNames).build();
    }

    @GET
    @Path("catalog/{cName}")
    @Produces("application/json")
    public Response getCatalog(@PathParam("cName") String cName) {
        Catalog catalog = vdc.getCatalogByName(cName);

        // Set 'null' to all lists of distributions and violations. They should not be included here.
        if (catalog != null) {
            catalog.getDatasets().forEach(dataset -> {
                dataset.setDistributions(null);
                dataset.setViolations(null);
            });
            return Response.ok().entity(catalog).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("catalog/{cName}/dataset")
    @Produces("application/json")
    public Response getDatasetNamesOfCatalog(@PathParam("cName") String cName) {
        List<String> datasetNames = vdc.listDatasetNamesByCatalogName(cName);
        return Response.ok().entity(datasetNames).build();
    }

    @GET
    @Path("dataset")
    @Produces("application/json")
    public Response getAllDatasets(@QueryParam("rows") @DefaultValue("300") int rows,
                                   @QueryParam("start") @DefaultValue("0") int start) {
        List<String> datasetNames = vdc.listAllDatasetNamesWithPaging(start, rows);
        return Response.ok().entity(datasetNames).build();
    }

    @GET
    @Path("dataset/{dName}")
    @Produces("application/json")
    public Response getDatasetOfCatalog(@PathParam("dName") String dName) {
        Dataset dataset = vdc.getDatasetByName(dName);
        if (dataset != null) {
            return Response.ok().entity(dataset).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("similarity/{datasetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSimilaritiesForUri(
            @PathParam("datasetId") String datasetId,
            @QueryParam("limit") @DefaultValue("0") int limit) {

        String uri = "http://europeandataportal.eu/set/data/" + datasetId;
        String similarities = similarityService.getSimilars(uri, limit);

        if (similarities != null) {
            return Response.ok(similarities).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("report/{langCode}/{format}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getMqaReport(
            @PathParam("langCode") String langCode,
            @PathParam("format") String format) {

        boolean isRequestValid = langCode != null
                && format != null;

        if (isRequestValid) {
            try {
                ReportHandler.ReportFormat reportFormat = ReportHandler.ReportFormat.valueOf(format.toUpperCase());

                String fileName = String.format("mqa-report_%s.%s", langCode.toLowerCase(), reportFormat.name().toLowerCase());
                java.nio.file.Path reportPath = Paths.get(reportDirectory, fileName);
                LOG.info("Download request for file [{}]", reportPath.toAbsolutePath());

                if (Files.exists(reportPath) && Files.isRegularFile(reportPath)) {
                    return Response.ok(reportPath.toFile())
                            .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"")
                            .header("Content-Type", reportFormat.getMimeType())
                            .build();
                } else {
                    return Response.status(Response.Status.NOT_FOUND).build();
                }
            } catch (IllegalArgumentException e) {
                // is thrown when no enum could be created from the format string
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
