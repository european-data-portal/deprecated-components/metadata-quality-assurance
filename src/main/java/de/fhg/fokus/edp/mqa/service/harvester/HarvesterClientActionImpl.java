package de.fhg.fokus.edp.mqa.service.harvester;

import com.fasterxml.jackson.databind.JsonNode;
import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.rest.Authenticator;
import de.fhg.fokus.edp.mqa.service.utils.MapperUtils;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

/**
 * Created by bdi on 22/07/16.
 */
@ApplicationScoped
@Named
public class HarvesterClientActionImpl implements Serializable {

	@Inject
	private MapperUtils mapperUtils;

	@Inject
	@Log
	private Logger LOG;

	@Inject
	private Authenticator authenticator;

	private HarvesterClientAction harvesterClientAction;

	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.HARVESTER_URL_REST)
	private String harvesterRestUrl;
	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.CKAN_BASICAUTH_PASSWORD)
	private String basicAuthPassword;
	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.CKAN_BASICAUTH_USERNAME)
	private String basicAuthUsername;

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        authenticator.setPassword(basicAuthPassword);
        authenticator.setUsername(basicAuthUsername);
        ResteasyClient client = new ResteasyClientBuilder().connectionPoolSize(20).build().register(authenticator);
        ResteasyWebTarget target = client.target(harvesterRestUrl);
        harvesterClientAction = target.proxy(HarvesterClientAction.class);
    }

    public Repository retrieveRepository(String repositoryName) {
        Repository repository = null;

        Response response = harvesterClientAction.repositories();
        if (response.getStatus() == 200) {
            JsonNode results = mapperUtils.getSearchResult(mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(response)));

            for (JsonNode nodeRepository : results) {
                if (nodeRepository.get("name").asText().equals(repositoryName)) {
                    repository = new Repository();
                    repository.setName(nodeRepository.get("name").textValue());
                    repository.setType(nodeRepository.get("type").textValue());
                    repository.setIncremental(nodeRepository.get("incremental").asBoolean());
                    repository.setId(nodeRepository.get("id").asLong());
                    repository.setSourceHarvester(nodeRepository.get("sourceHarvester").get(0).asLong());
                    try {
                        repository.setHomepage(new URL(nodeRepository.get("homepage").asText()));
                    } catch (MalformedURLException e) {
                        repository.setHomepage(null);
                    }
                    repository.setLanguage(nodeRepository.get("language").asText());
                    repository.setPublisher(nodeRepository.get("publisher").asText());
                    repository.setPublisherEmail(nodeRepository.get("publisherEmail").asText());
                    break;
                }
            }
        } else {
            LOG.warn("Attempt to retrieve repositories returned status code [{}].", response.getStatus());
        }

        return repository;
    }

	public Harvester searchHarvester(long harvesterId) {
        Harvester harvester = null;

        Response response = harvesterClientAction.harvester(harvesterId);
        if (response.getStatus() == 200) {
            JsonNode result = mapperUtils.getSearchResult(mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(response)));

            harvester = new Harvester();
            harvester.setId(result.get("id").asLong());
            harvester.setFrequency(result.get("frequency").asText());
            harvester.setScript(result.get("script").asText());
            harvester.setScheduled(LocalDateTime.ofInstant(Instant.ofEpochMilli(result.get("scheduled").asLong()), TimeZone.getDefault().toZoneId()));
        } else {
            LOG.warn("Attempt to retrieve harvester with ID [{}] returned status code [{}].", harvesterId, response.getStatus());
        }

        return harvester;
    }

    public Run retrieveLastRun(long harvesterId) {
        Run run = null;

        Response response = harvesterClientAction.runs(harvesterId);
        if (response.getStatus() == 200) {
            JsonNode result = mapperUtils.getSearchResult(mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(response)));
            JsonNode nodeRun = result.get(0);
            if (nodeRun != null) {
                run = new Run();
                run.setId(nodeRun.get("id").asLong());
                run.setStatus(nodeRun.get("status").asText());
                try {
                    run.setStartTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(nodeRun.get("startTime").asLong()), TimeZone.getDefault().toZoneId()));
                } catch (NullPointerException e) {
                    run.setStartTime(null);
                }
                if (nodeRun.get("endTime").asLong() != 0) {
                    run.setEndtTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(nodeRun.get("endTime").asLong()), TimeZone.getDefault().toZoneId()));
                    run.setDuration(Duration.between(run.getStartTime(), run.getEndtTime()));
                } else {
                    run.setEndtTime(null);
                    run.setDuration(null);
                }
                run.setNumberAdded(nodeRun.get("numberAdded").asInt());
                run.setNumberDeleted(nodeRun.get("numberDeleted").asInt());
                run.setNumberUpdated(nodeRun.get("numberUpdated").asInt());
                run.setNumberSkipped(nodeRun.get("numberSkipped").asInt());
                run.setNumberRejected(nodeRun.get("numberRejected").asInt());
            }
        } else {
            LOG.warn("Attempt to retrieve last run for harvester with ID [{}] returned status code [{}].", harvesterId, response.getStatus());
        }

        return run;
    }

    public List<RunLog> retrieveRunLogs(long harvesterId, long runId, long start, long rows) {
        List<RunLog> logs = null;
        Response response = harvesterClientAction.logs(harvesterId, runId, start, rows);

        if (response.getStatus() == 200) {
            JsonNode result = mapperUtils.getSearchResult(mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(response)));

            logs = new ArrayList<>();

            for (JsonNode nodeLog : result) {
                RunLog log = new RunLog();
                log.setId(nodeLog.get("id").asLong());
                log.setAttachment(nodeLog.get("attachment").asBoolean());
                log.setMessage(nodeLog.get("message").asText());
                log.setSeverity(RunLog.Severity.valueOf(nodeLog.get("severity").asText()));
                log.setTimestamp(LocalDateTime.ofInstant(Instant.ofEpochMilli(nodeLog.get("created").asLong()), TimeZone.getDefault().toZoneId()));
                log.setCategory(RunLog.Category.valueOf(nodeLog.get("category").asText()));
                log.setStage(RunLog.ServiceStage.valueOf(nodeLog.get("stage").asText()));
                logs.add(log);
            }
        } else {
            LOG.warn("Attempt to retrieve logs for run with ID [{}] for harvester with id [{}] returned status code [{}].", runId, harvesterId, response.getStatus());
        }

        return logs;
    }

    public String retrieveAttachment(long harvesterId, long runId, long logId) {
        Response response = harvesterClientAction.attachment(harvesterId, runId, logId);

        if (response.getStatus() == 200) {
            return mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(response)).toString();
        } else {
            LOG.warn("Attempt to retrieve attachment for log with ID [{}] of run with ID [{}] for harvester with id [{}] returned status code [{}].", logId, runId, harvesterId, response.getStatus());
            return ResourceBundle.getBundle(Constants.MSG_BUNDLE, FacesContext.getCurrentInstance().getViewRoot().getLocale()).getString("harvester.run.log.noattachment");
        }
    }

    public long countRunLogs(long harvesterId, long runId) {
        long count = 0;
        Response response = harvesterClientAction.logs(harvesterId, runId, 0, 0);

        if (response.getStatus() == 200) {
            JsonNode result = mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(response));
            count = result.get("total").asLong();
        } else {
            LOG.warn("Attempt to retrieve logs for run with ID [{}] for harvester with id [{}] returned status code [{}].", runId, harvesterId, response.getStatus());
        }

        return count;
    }
}
