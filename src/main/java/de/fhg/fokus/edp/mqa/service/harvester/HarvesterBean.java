package de.fhg.fokus.edp.mqa.service.harvester;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.namedBeans.CurrentCatalogBean;
import de.fhg.fokus.edp.mqa.service.utils.MapperUtils;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by bdi on 26/07/16.
 */
@ViewScoped
@Named
public class HarvesterBean implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private HarvesterClientActionImpl harvesterClientActionImpl;

    @Inject
    private CurrentCatalogBean currentCatalogBean;

    @Inject
    private MapperUtils mapperUtils;

    private Repository currentRepository;
    private Harvester currentHarvester;
    private Run currentRun;
    private List<RunLog> logs;
    private String selectedAttachment;

    private static final long LOG_PAGE_SIZE = 30L;
    private long logTotal = 0L;
    private long logStart = 0L;

    @PostConstruct
    public void init() {

        if (currentCatalogBean != null && currentCatalogBean.getCatalogName() != null) {
            currentRepository = harvesterClientActionImpl.retrieveRepository(currentCatalogBean.getCatalogTitle());

            if (currentRepository != null) {
                currentHarvester = harvesterClientActionImpl.searchHarvester(currentRepository.getSourceHarvester());

                if (currentHarvester != null) {
                    currentRun = harvesterClientActionImpl.retrieveLastRun(currentHarvester.getId());

                    if (currentRun != null) {
                        logs = harvesterClientActionImpl.retrieveRunLogs(currentHarvester.getId(), currentRun.getId(), logStart, LOG_PAGE_SIZE);
                        logTotal = harvesterClientActionImpl.countRunLogs(currentHarvester.getId(), currentRun.getId());
                    }
                }
            }
        }
    }

    public void previousLogPage() {
        logStart -= LOG_PAGE_SIZE;
        updateLogs();
    }

    public void nextLogPage() {
        logStart += LOG_PAGE_SIZE;
        updateLogs();
    }

    private void updateLogs() {
        if (currentHarvester != null && currentRun != null)
            logs = harvesterClientActionImpl.retrieveRunLogs(currentHarvester.getId(), currentRun.getId(), logStart, LOG_PAGE_SIZE);
    }

    public void updateAttachment(long logId) {
        selectedAttachment = harvesterClientActionImpl.retrieveAttachment(currentHarvester.getId(), currentRun.getId(), logId);
    }

    public String getPrettyPrintedAttachment() {
        return selectedAttachment != null ? mapperUtils.prettyPrintJsonString(selectedAttachment) : null;
    }

    public List<RunLog> getLogs() {
        return logs;
    }

    public void setLogs(List<RunLog> logs) {
        this.logs = logs;
    }

    public Repository getCurrentRepository() {
        return currentRepository;
    }

    public void setCurrentRepository(Repository currentRepository) {
        this.currentRepository = currentRepository;
    }

    public Harvester getCurrentHarvester() {
        return currentHarvester;
    }

    public void setCurrentHarvester(Harvester currentHarvester) {
        this.currentHarvester = currentHarvester;
    }

    public Run getCurrentRun() {
        return currentRun;
    }

    public void setCurrentRun(Run currentRun) {
        this.currentRun = currentRun;
    }

    public String getSelectedAttachment() {
        return selectedAttachment;
    }

    public void setSelectedAttachment(String selectedAttachment) {
        this.selectedAttachment = selectedAttachment;
    }

    public long getLogStart() {
        return logStart;
    }

    public void setLogStart(long logStart) {
        this.logStart = logStart;
    }

    public long getLogTotal() {
        return logTotal;
    }

    public void setLogTotal(long logTotal) {
        this.logTotal = logTotal;
    }

    public static long getLogPageSize() {
        return LOG_PAGE_SIZE;
    }
}