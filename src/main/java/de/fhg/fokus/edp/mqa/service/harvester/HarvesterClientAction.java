package de.fhg.fokus.edp.mqa.service.harvester;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by bdi on 22/07/16.
 */
public interface HarvesterClientAction {

	@GET
	@Produces("application/json")
    @Consumes("application/json")
	@Path("/rest/repository")
	Response repositories();

	@GET
	@Produces("application/json")
	@Path("/rest/repository/{repositoryId}")
	Response repository(@PathParam("repositoryId") long repositoryId);

	@GET
	@Produces("application/json")
    @Consumes("application/json")
	@Path("/rest/harvester/{harvesterId}")
	Response harvester(@PathParam("harvesterId") long harvesterId);
        
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/rest/harvester")
	Response harvesterSearch(@QueryParam("harvesterTitle") String harvesterTitle);

	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/rest/harvester/{harvesterId}/run")
	Response runs(@PathParam("harvesterId") long harvesterId);

	@GET
	@Produces("application/json")
    @Consumes("application/json")
	@Path("/rest/harvester/{harvesterId}/run/{runId}/log")
	Response logs(@PathParam("harvesterId") long harvesterId, @PathParam("runId") long runId,
			@QueryParam("start") long start, @QueryParam("rows") long rows);

	@GET
	@Produces("application/json")
    @Consumes("application/json")
	@Path("/rest/harvester/{harvesterId}/run/{runId}/log/{logId}/attachment")
	Response attachment(@PathParam("harvesterId") long harvesterId, @PathParam("runId") long runId,
			@PathParam("logId") long logId);
}
