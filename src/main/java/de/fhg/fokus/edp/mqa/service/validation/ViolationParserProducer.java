package de.fhg.fokus.edp.mqa.service.validation;


import de.fhg.fokus.edp.mqa.service.ckan.ViolationParserJsonImpl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.New;
import javax.enterprise.inject.Produces;
import java.io.Serializable;

/**
 * Created by bdi on 02/06/15.
 */
@ApplicationScoped
public class ViolationParserProducer implements Serializable {

    /**
     * Gets violation parser.
     *
     * @param violationParserJsonImpl the violation parser json
     * @return the violation parser
     */
    @Produces
    @ViolationEngine(ViolationParserType.JSON_SCHEMA)
    public ViolationParser getViolationParser(@New ViolationParserJsonImpl violationParserJsonImpl) {
        return violationParserJsonImpl;
    }
}