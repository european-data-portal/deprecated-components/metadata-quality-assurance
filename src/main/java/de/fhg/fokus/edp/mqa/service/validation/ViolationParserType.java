package de.fhg.fokus.edp.mqa.service.validation;

/**
 * Created by bdi on 02/06/15.
 */
public enum ViolationParserType {
    /**
     * Rdf xml dcatap violation parser type.
     */
    RDF_XML_DCATAP,
    /**
     * Json schema violation parser type.
     */
    JSON_SCHEMA
}
