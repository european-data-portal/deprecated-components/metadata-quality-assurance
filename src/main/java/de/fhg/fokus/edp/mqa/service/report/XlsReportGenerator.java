package de.fhg.fokus.edp.mqa.service.report;

import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;

/**
 * Created by fritz on 16.11.16.
 */
@RequestScoped
@Named
public class XlsReportGenerator extends ReportGenerator {

    @Override
    void generateReport(String language) {

        reloadLanguageSpecificData();

        // create workbook
        Workbook xlsReport = new HSSFWorkbook();

        // dashboard sheet
        org.apache.poi.ss.usermodel.Sheet dashboardSheet = xlsReport.createSheet(dashboardOverview);
        dashboardSheet.createRow(0).createCell(0).setCellValue(dashboardTitle);
        Row runRow = dashboardSheet.createRow(1);
        runRow.createCell(0).setCellValue(dashboardLastUpdate);
        runRow.createCell(1).setCellValue(lastRun);
        Row dateRow = dashboardSheet.createRow(2);
        dateRow.createCell(0).setCellValue(dashboardCurrentDate);
        dateRow.createCell(1).setCellValue(currentDate);

        int rowOffset = 4;

        // distribution statistics
        if (renderGlobalDistributions) {
            writeXlsLine(dashboardSheet, 0, 6, dashboardDistributionsDiagramsBean.getDistributionsUrlPieChart(), distAccessibilityAll, "%");
            writeXlsLine(dashboardSheet, 0, 10, dashboardDistributionsDiagramsBean.getDistributionsStatusPieChart(), distStatusCodes, null);
            writeXlsLine(dashboardSheet, 0, 14, dashboardDistributionsDiagramsBean.getDistributionsWithoutDownloadUrl(), distWithoutDownloadUrl, "%");
            writeXlsLine(dashboardSheet, 0, 18, dashboardDistributionsDiagramsBean.getDistributionsCatalogAvailabilityBarChart(), distAccessibilityCatalog, "%");
            writeXlsLine(dashboardSheet, 0, 22, dashboardDistributionsDiagramsBean.getDistributionsMachineReadablePercentagePieChart(), distMachineReadablePercentage, "%");
            writeXlsLine(dashboardSheet, 0, 26, dashboardDistributionsDiagramsBean.getMostUsedDistributionFormatsColumnChart(), distFormatMostUsed, "%");
            writeXlsLine(dashboardSheet, 0, 30, dashboardDistributionsDiagramsBean.getMachineReadableDistributionCountPerCatalogBarChart(), distMachineReadableCount, "%");
            rowOffset = 34;
        }

        // Violations statistics
        if (renderGlobalViolations) {
            writeXlsLine(dashboardSheet, 0, rowOffset, dashboardViolationsDiagramsBean.getViolationMostOccurredPieChart(), violationMostOccurred, null);
            writeXlsLine(dashboardSheet, 0, rowOffset + 4, dashboardViolationsDiagramsBean.getDatasetsConformPieChart(), violationComplianceAll, "%");
            writeXlsLine(dashboardSheet, 0, rowOffset + 8, dashboardViolationsDiagramsBean.getViolationsCountByCatalogBarChart(), violationComplianceCatalog, "%");
            rowOffset = 46;
        }

        // Licence statistics
        if (renderGlobalLicences) {
            writeXlsLine(dashboardSheet, 0, rowOffset, dashboardLicencesDiagramsBean.getKnownLicencesPercentagePieChart(), licenceKnownPercentage, "%");
            writeXlsLine(dashboardSheet, 0, rowOffset + 4, dashboardLicencesDiagramsBean.getMostUsedLicencesColumnChart(), licenceMostUsed, "%");
            writeXlsLine(dashboardSheet, 0, rowOffset + 8, dashboardLicencesDiagramsBean.getKnownLicencesByCatalogBarChart(), licenceCatalogsWithMostKnown, "%");
        }

        for (Catalog catalog : catalogs) {
            // rid catalog name of special chars
            String catalogName = WorkbookUtil.createSafeSheetName(catalog.getTitle());
            org.apache.poi.ss.usermodel.Sheet catalogSheet = xlsReport.createSheet(catalogName);
            catalogSheet.createRow(0).createCell(0).setCellValue(catalogName);
            reloadCatalogDiagrams(String.valueOf(catalog.getId()));

            // store offset to prevent empty blocks when not all data is available
            rowOffset = 4;

            // Distributions statistics
            if (catalogDashboardDistributionBean.renderSection()) {
                writeXlsLine(catalogSheet, 0, 4, catalogDashboardDistributionBean.getAvailableDistributionsPercentagePieChart(), distAccessibilityAll, "%");
                writeXlsLine(catalogSheet, 0, 8, catalogDashboardDistributionBean.getErrorCodesPieChart(), distStatusCodes, null);
                writeXlsLine(catalogSheet, 0, 12, catalogDashboardDistributionBean.getWithDownloadUrlPieChart(), distWithoutDownloadUrl, "%");
                writeXlsLine(catalogSheet, 0, 16, catalogDashboardDistributionBean.getMachineReadableDatasetPieChart(), distMachineReadablePercentage, "%");
                writeXlsLine(catalogSheet, 0, 20, catalogDashboardDistributionBean.getMostUsedFormats(), distFormatMostUsed, "%");
                rowOffset = 24;
            }

            // Violations statistics
            if (catalogDashboardViolationBean.renderSection()) {
                writeXlsLine(catalogSheet, 0, rowOffset, catalogDashboardViolationBean.getMostOccurredViolationsPieChart(), violationMostOccurred, null);
                writeXlsLine(catalogSheet, 0, rowOffset + 4, catalogDashboardViolationBean.getConformDatasetPieChart(), violationComplianceAll, "%");
                rowOffset = 32;
            }

            // Licence statistics
            if (catalogDashboardLicenceBean.renderSection()) {
                writeXlsLine(catalogSheet, 0, rowOffset, catalogDashboardLicenceBean.getRatioKnownUnknownPieChart(), licenceKnownPercentage, "%");
                writeXlsLine(catalogSheet, 0, rowOffset + 4, catalogDashboardLicenceBean.getMostUsedLicencesColumnChart(), licenceMostUsed, "%");
            }
        }

        try {
            // save workbook to disk
            setReportFile(ReportHandler.ReportFormat.XLS, language);
            FileOutputStream fileOut = new FileOutputStream(reportFile);
            xlsReport.write(fileOut);
            fileOut.close();
            LOG.info("Created XLS report file: [{}]", reportFile.getAbsolutePath());
        } catch (IOException e) {
            LOG.error("Failed to generate XLS report for language [{}].", language, e);
        }
    }

    private void writeXlsLine(org.apache.poi.ss.usermodel.Sheet sheet, int startCol, int startRow, Set<ReportElementBean> reportElementBeans, String description, String unit) {
        // set description
        sheet.createRow(startRow).createCell(startCol).setCellValue(description);

        if (unit == null || unit.isEmpty())
            unit = "";

        // set values
        Row labelRow = sheet.createRow(startRow + 1);
        Row valueRow = sheet.createRow(startRow + 2);
        for (Object reportElementBean : reportElementBeans) {
            ReportElementBean element = (ReportElementBean) reportElementBean;
            labelRow.createCell(startCol).setCellValue(element.getLabel());
            valueRow.createCell(startCol).setCellValue(element.getValue() + " " + unit);
            startCol++;
        }
    }
}
