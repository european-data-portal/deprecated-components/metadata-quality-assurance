package de.fhg.fokus.edp.mqa.service.namedBeans.converter;

import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.namedBeans.CatalogsBean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by bdi on 01/11/15.
 */
@Named
public class CatalogConverter implements Converter {

    /**
     * The Catalogs bean.
     */
    @Inject
    CatalogsBean catalogsBean;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        Catalog catalogObject = null;

        for (Catalog catalog : catalogsBean.getCatalogs()) {
            if (catalog.getName().equals(value)) {
                catalogObject = catalog;
            }
        }

        return catalogObject;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        return (String) value;
    }
}
