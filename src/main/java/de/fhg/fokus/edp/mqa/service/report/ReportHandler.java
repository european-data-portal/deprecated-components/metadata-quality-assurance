package de.fhg.fokus.edp.mqa.service.report;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.utils.LocaleUtil;
import org.slf4j.Logger;

import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Map;

/**
 * Created by bdi on 24/03/16.
 */
@Dependent
@Named
public class ReportHandler {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private LocaleUtil localeUtil;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.REPORT_LANGUAGES)
    private String reportLanguages;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.REPORT_DIRECTORY)
    String reportDirectory;

    @Inject
    private XlsReportGenerator xlsReportGenerator;

    @Inject
    private OdsReportGenerator odsReportGenerator;

    @Inject
    private PdfReportGenerator pdfReportGenerator;

    @Inject
    private ChartGenerator chartGenerator;

    public void generateReports() {

        if (!reportLanguages.isEmpty()) {

            // generate reports for all languages
            for (String language : reportLanguages.split(",")) {

                language = language.trim();
                Locale locale = new Locale(language);
                localeUtil.setCurrentLocale(locale);

                // generate reports
                xlsReportGenerator.generateReport(language);
                odsReportGenerator.generateReport(language);

                if (chartGenerator.isChartRenderEngineAvailable()) {
                    pdfReportGenerator.generateReport(language);
                } else {
                    LOG.warn("Could not connect to chart render engine. Skipping PDF report");
                }
            }
        }
    }

    /**
     * Download file.
     *
     * @throws IOException the io exception
     */
    public void downloadFile(ReportFormat requestedFormat) throws IOException { // TODO: Exception handling

        Map<String, String> parameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String fileExtension = requestedFormat.name().toLowerCase();
        String fileName = String.format("mqa-report_%s.%s", parameterMap.get("lang"), fileExtension);

        InputStream fis = new FileInputStream(Paths.get(reportDirectory, fileName).toFile());

        // Prepare the response
        HttpServletResponse response = (HttpServletResponse) FacesContext
                .getCurrentInstance().getExternalContext().getResponse();
        response.setContentType(requestedFormat.getMimeType());
        response.setHeader("Content-Disposition", "attachment;filename=EuropeanDataPortal_MQA_Report_" + parameterMap.get("lang").toUpperCase() + "." + fileExtension);

        final byte[] buf = new byte[1024];
        int read;

        while ((read = fis.read(buf)) != -1) {
            response.getOutputStream().write(buf, 0, read);
        }

        fis.close();

        response.getOutputStream().flush();
        response.getOutputStream().close();

        FacesContext.getCurrentInstance().responseComplete();
    }

    public enum ReportFormat {
        ODS("Open Document Spreadsheet (.ods)", "application/vnd.oasis.opendocument.spreadsheet"),
        XLS("Microsoft Excel (.xls)", "application/vnd.ms-excel"),
        PDF("Portable Document Format (.pdf)", "application/pdf");

        private final String label;
        private final String mimeType;

        ReportFormat(String label, String mimeType) {
            this.label = label;
            this.mimeType = mimeType;
        }

        public String getLabel() {
            return label;
        }

        public String getMimeType() {
            return mimeType;
        }
    }

    public ReportFormat[] getReportFormats() {
        return ReportFormat.values();
    }

    public String getReportLanguages() {
        return reportLanguages;
    }
}