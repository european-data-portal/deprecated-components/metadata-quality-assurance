package de.fhg.fokus.edp.mqa.service.report;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by fritz on 16.11.16.
 */
@RequestScoped
@Named
public class OdsReportGenerator extends ReportGenerator {

    @Override
    void generateReport(String language) {

        try {
            // read xls file
            File xlsFile = Paths.get(reportDirectory, "mqa-report_" + language + ".xls").toFile();
            NPOIFSFileSystem fs = new NPOIFSFileSystem(xlsFile);
            HSSFWorkbook xlsReport = new HSSFWorkbook(fs.getRoot(), true);

            // get row and column counts
            int maxRowCount = 0;
            int maxColumnCount = 0;
            for (org.apache.poi.ss.usermodel.Sheet sheet : xlsReport) {
                maxRowCount = sheet.getLastRowNum() + 1 > maxRowCount ? sheet.getLastRowNum() + 1 : maxRowCount;

                for (Row row : sheet)
                    maxColumnCount = row.getLastCellNum() > maxColumnCount ? row.getLastCellNum() : maxColumnCount;
            }

            // create ods workbook
            SpreadSheet odsReport = SpreadSheet.create(xlsReport.getNumberOfSheets(), maxColumnCount, maxRowCount);

            // copy each sheet to ods file
            for (int sheetNumber = 0; sheetNumber < xlsReport.getNumberOfSheets(); sheetNumber++) {
                org.apache.poi.ss.usermodel.Sheet xlsSheet = xlsReport.getSheetAt(sheetNumber);
                Sheet odsSheet = odsReport.getSheet(sheetNumber);
                odsSheet.setName(xlsSheet.getSheetName());

                for (Row row : xlsSheet) {
                    for (int columnNumber = row.getFirstCellNum(); columnNumber < row.getLastCellNum(); columnNumber++) {
                        // copy cell value to according xls cell
                        odsSheet.setValueAt(row.getCell(columnNumber).getStringCellValue(), columnNumber, row.getRowNum());
                    }
                }
            }

            // Save the report to an ODS tmp file
            setReportFile(ReportHandler.ReportFormat.ODS, language);
            odsReport.saveAs(reportFile);
            LOG.info("Created ODS report file: {}", reportFile.getAbsolutePath());
        } catch (IOException e) {
            LOG.error("Failed to generate ODS report for language [{}].", language, e);
        }
    }
}
