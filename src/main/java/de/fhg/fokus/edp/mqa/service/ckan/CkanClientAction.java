package de.fhg.fokus.edp.mqa.service.ckan;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by bdi on 24/04/15.
 */
public interface CkanClientAction {

    /**
     * Package search response.
     *
     * @param query the query
     * @param rows  the rows
     * @param start the start
     * @return the response
     */
    @GET
    @Produces("application/json")
    @Path("/api/3/action/package_search")
    Response packageSearch(@QueryParam("q") String query, @QueryParam("rows") int rows, @QueryParam("start") int start);

    /**
     * Package get all response.
     *
     * @param rows  the rows
     * @param start the start
     * @return the response
     */
    @GET
    @Produces("application/json")
    @Path("/api/3/action/package_search")
    Response packageGetAll(@QueryParam("rows") int rows, @QueryParam("start") int start);

    /**
     * Package show response.
     *
     * @param packageName the package name
     * @return the response
     */
    @GET
    @Produces("application/json")
    @Path("/api/3/action/package_show")
    Response packageShow(@QueryParam("id") String packageName);

    /**
     * Organization list response.
     *
     * @return the response
     */
    @GET
    @Produces("application/json")
    @Path("/api/3/action/organization_list")
    Response organizationList();

    /**
     * Organization show response.
     *
     * @param body the body
     * @return the response
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/api/3/action/organization_show")
    Response organizationShow(String body);

    /**
     * Resource show response.
     *
     * @param resourceId the resource id
     * @return the response
     */
    @GET
    @Produces("application/json")
    @Path("/api/3/action/resource_show")
    Response resourceShow(@QueryParam("id") String resourceId);

    @GET
    @Produces("application/json")
    @Path("/api/3/action/license_list")
    Response licenceList();
}
