package de.fhg.fokus.edp.mqa.service.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Path("/callback")
public class UrlCheckCallback {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{distributionId}")
    public Response distributionUrlCheckCallback(@PathParam("distributionId") String distributionId, String body) {
        LOG.debug("Callback for Distribution with ID [{}]", distributionId);

        Distribution distribution = vdc.getDistributionByInstanceId(distributionId);

        if (distribution != null) {
            try {
                List<UrlCheckResponse> responses = Arrays.asList(new ObjectMapper().readValue(body, UrlCheckResponse[].class));

                String accessUrl = distribution.getAccessUrl();
                UrlCheckResponse accessResponse = responses.stream()
                        .filter(res -> res.getUrl().equals(accessUrl))
                        .findAny()
                        .orElse(null);

                String downloadUrl = distribution.getDownloadUrl();
                UrlCheckResponse downloadResponse = responses.stream()
                        .filter(res -> res.getUrl().equals(downloadUrl))
                        .findAny()
                        .orElse(null);

                boolean hasChanges = false;

                if (accessResponse != null) {

                    if (distribution.getStatusAccessUrl() != accessResponse.getStatusCode()) {
                        distribution.setStatusAccessUrl(accessResponse.getStatusCode());

                        if (distribution.getStatusAccessUrl() != Constants.OK)
                            distribution.setAccessErrorMessage(accessResponse.getMessage());

                        distribution.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
                        hasChanges = true;
                    }

                    if (!StringUtils.equals(distribution.getMediaTypeChecked(), accessResponse.getMimeType())) {
                        distribution.setMediaTypeChecked(accessResponse.getMimeType());
                        hasChanges = true;
                    }

                    if (downloadResponse != null) {
                        if (distribution.getStatusDownloadUrl() != downloadResponse.getStatusCode()) {
                            distribution.setStatusDownloadUrl(downloadResponse.getStatusCode());
                            distribution.setDownloadErrorMessage(downloadResponse.getMessage());
                            distribution.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
                            hasChanges = true;
                        }
                    }

                    if (hasChanges)
                        vdc.updateEntity(distribution);

                } else {
                    LOG.warn("URL check response did not contain results for access url [{}] of distribution with ID [{}]. Content: {}",
                            distribution.getAccessUrl(), distributionId, body);
                }
            } catch (IOException e) {
                LOG.warn("Failed to read URL check response JSON: {}", e.getMessage());
            }
        } else {
            LOG.warn("Received callback for distribution with ID [{}], but no distribution was found with this ID", distributionId);
        }

        return Response.ok().build();
    }

    private static class UrlCheckResponse {

        private String url;

        private Integer statusCode;

        private String mimeType;

        private String message;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
        }

        public String getMimeType() {
            return mimeType;
        }

        public void setMimeType(String mimeType) {
            this.mimeType = mimeType;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
