package de.fhg.fokus.edp.mqa.service.namedBeans.converter;

import org.ocpsoft.urlbuilder.util.Encoder;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

/**
 * Created by bdi on 18/06/15.
 */
@Named
public class DistributionUrlConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return s;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return Encoder.path((String) o);
    }
}
