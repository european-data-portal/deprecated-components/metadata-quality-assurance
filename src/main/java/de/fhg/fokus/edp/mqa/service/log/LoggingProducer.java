package de.fhg.fokus.edp.mqa.service.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.io.Serializable;

/**
 * Created by bdi on 06/08/15.
 */
@ApplicationScoped
public class LoggingProducer implements Serializable {

    @Produces
    @Log
    private Logger createLogger(InjectionPoint injectionPoint) {
        return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass());
    }
}
