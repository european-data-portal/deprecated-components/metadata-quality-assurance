package de.fhg.fokus.edp.mqa.service.similarities;

import de.fhg.fokus.edp.mqa.service.model.DatasetSimilarity;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Created by bdi on 05/10/15.
 */
@Dependent
public class SimilarityOutputfileParser {

    /**
     * The Vdc.
     */
    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    ValidationDataClient vdc;

    /**
     * Parse and persist.
     *
     * @param file the file
     * @throws IOException the io exception
     */
    public void parseAndPersist(String file) throws IOException {
        final int DATASET_INSTANCEID_A = 1;
        final int DATASET_INSTANCEID_B = 1;
        final int SCORE = 0;

        Reader in = new FileReader(file);

        for (CSVRecord record : CSVFormat.TDF.parse(in)) {
            DatasetSimilarity datasetSimilarity = vdc.createDatasetSimilarity();
            datasetSimilarity.setDatasetInstanceIdA(record.get(DATASET_INSTANCEID_A));
            datasetSimilarity.setDatasetInstanceIdB(record.get(DATASET_INSTANCEID_B));
            datasetSimilarity.setScore(Double.parseDouble(record.get(SCORE)));
            if (!vdc.isDatasetSimilarityExisting(datasetSimilarity)) {
                vdc.persistEntity(datasetSimilarity);
            } else {
                // FIXME: Might break because of detached entity
                vdc.updateEntity(datasetSimilarity);
            }
        }
    }
}
