package de.fhg.fokus.edp.mqa.service.duplicates;

/**
 * Created by fritz on 17.03.17.
 */
class LanguageFailedException extends Exception {

    LanguageFailedException() {}

    LanguageFailedException(String languageCode) {
        super("Processing language " + languageCode + " did not finish successfully");
    }
}
