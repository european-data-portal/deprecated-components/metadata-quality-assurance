package de.fhg.fokus.edp.mqa.service.validation;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by bdi on 10/04/15.
 */
@Qualifier
@Retention(RUNTIME)
@Target({FIELD, TYPE, METHOD})
public @interface ValidationData {

    /**
     * Value validation data client type.
     *
     * @return the validation data client type
     */
    ValidationDataClientType value();
}
