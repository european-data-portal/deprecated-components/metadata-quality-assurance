package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.log.Log;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import static de.fhg.fokus.edp.mqa.service.config.Constants.SCHEDULER_VALIDATION_JOB;
import static de.fhg.fokus.edp.mqa.service.config.Constants.SCHEDULER_VALIDATION_JOB_INSTANT;

/**
 * Created by bdi on 12/10/15.
 */
@ApplicationScoped
public class MqaTriggerListener implements TriggerListener {

    private static final String TRIGGER_LISTENER_NAME = "GlobalTriggerListener";
    @Inject
    @Log
    private Logger LOG;

    @Inject
    private MqaScheduler mqaScheduler;

    @Override
    public String getName() {
        return TRIGGER_LISTENER_NAME;
    }

    @Override
    public void triggerFired(Trigger trigger, JobExecutionContext context) {
        LOG.info("Fired trigger {} with job {} at {}", trigger.getKey(), trigger.getJobKey(), trigger.getStartTime());
    }

    @Override
    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
        return false;
    }

    @Override
    public void triggerMisfired(Trigger trigger) {
        LOG.warn("Misfire of trigger {} with job {} at {}", trigger.getKey(), trigger.getJobKey(), trigger.getStartTime());
    }

    @Override
    public void triggerComplete(Trigger trigger, JobExecutionContext context, Trigger.CompletedExecutionInstruction triggerInstructionCode) {
        LOG.info("{} done in {}", context.getJobDetail().getKey().getName(), Duration.of(context.getJobRunTime(), ChronoUnit.MILLIS));
        String jobDone = trigger.getJobKey().getName();

        if (StringUtils.equals(jobDone, SCHEDULER_VALIDATION_JOB) || StringUtils.equals(jobDone, SCHEDULER_VALIDATION_JOB_INSTANT)) {
            try {
                mqaScheduler.runDeletionJobNow();
                mqaScheduler.runReportJobNow();
            } catch (SchedulerException e) {
                LOG.error("Error while getting report job", e);
            }
        }
    }
}
