package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.Licence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by fritz on 30.09.16.
 */
@Entity
@Table(name = "licence")
@NamedQueries({
        @NamedQuery(name = "Licence.getLicenceById", query = "SELECT l FROM LicenceEntity l WHERE l.licenceId = :licenceId"),
        @NamedQuery(name = "Licence.getAll", query = "SELECT l FROM LicenceEntity l"),
        @NamedQuery(name = "Licence.getAllCount", query = "SELECT COUNT(l) FROM LicenceEntity l"),
})
public class LicenceEntity implements Licence {

    private static Logger LOG = LoggerFactory.getLogger(LicenceEntity.class);

    private static final int MAX_COLUMN_LENGTH_NAME = 1000;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(name="status", length = MAX_COLUMN_LENGTH_NAME)
    private String status;

    @Column(name="maintainer", length = MAX_COLUMN_LENGTH_NAME)
    private String maintainer;

    @Column(name="od_conformance", length = MAX_COLUMN_LENGTH_NAME)
    private String odConformance;

    @Column(name="family", length = MAX_COLUMN_LENGTH_NAME)
    private String family;

    @Column(name="title", length = MAX_COLUMN_LENGTH_NAME)
    private String title;

    @Column(name="url", length = MAX_COLUMN_LENGTH_NAME)
    private String url;

    @Column(name="osd_conformance", length = MAX_COLUMN_LENGTH_NAME)
    private String osdConformance;

    @Column(name="domain_data")
    private boolean domainData;

    @Column(name="domain_content")
    private boolean domainContent;

    @Column(name="okd_compliance")
    private boolean isOkdCompliant;

    @Column(name="osi_compliance")
    private boolean isOsiCompliant;

    @Column(name="generic")
    private boolean isGeneric;

    @Column(name="domain_software")
    private boolean domainSoftware;

    @Column(name="licence_id", length = MAX_COLUMN_LENGTH_NAME)
    private String licenceId;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    @PreRemove
    public void tearDown(){
        LOG.debug("Dataset [{}] is going to be removed...", this.family);
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getMaintainer() {
        return maintainer;
    }

    @Override
    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    @Override
    public String getOdConformance() {
        return odConformance;
    }

    @Override
    public void setOdConformance(String odConformance) {
        this.odConformance = odConformance;
    }

    @Override
    public String getFamily() {
        return family;
    }

    @Override
    public void setFamily(String family) {
        this.family = family;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getOsdConformance() {
        return osdConformance;
    }

    @Override
    public void setOsdConformance(String osdConformance) {
        this.osdConformance = osdConformance;
    }

    @Override
    public boolean isDomainData() {
        return domainData;
    }

    @Override
    public void setDomainData(boolean domainData) {
        this.domainData = domainData;
    }

    @Override
    public boolean isDomainContent() {
        return domainContent;
    }

    @Override
    public void setDomainContent(boolean domainContent) {
        this.domainContent = domainContent;
    }

    @Override
    public boolean isOkdCompliant() {
        return isOkdCompliant;
    }

    @Override
    public void setOkdCompliant(boolean okdCompliant) {
        isOkdCompliant = okdCompliant;
    }

    @Override
    public boolean isOsiCompliant() {
        return isOsiCompliant;
    }

    @Override
    public void setOsiCompliant(boolean osiCompliant) {
        isOsiCompliant = osiCompliant;
    }

    @Override
    public boolean isGeneric() {
        return isGeneric;
    }

    @Override
    public void setGeneric(boolean generic) {
        isGeneric = generic;
    }

    @Override
    public boolean isDomainSoftware() {
        return domainSoftware;
    }

    @Override
    public void setDomainSoftware(boolean domainSoftware) {
        this.domainSoftware = domainSoftware;
    }

    @Override
    public String getLicenceId() {
        return licenceId;
    }

    @Override
    public void setLicenceId(String licenceId) {
        this.licenceId = licenceId;
    }

    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }
}
