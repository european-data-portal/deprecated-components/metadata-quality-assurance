package de.fhg.fokus.edp.mqa.service.namedBeans.converter;

import de.fhg.fokus.edp.mqa.service.namedBeans.LanguageBean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Created by bdi on 15/06/15.
 */
@Named
public class DateTimeConverterMedium implements Converter {

    @Inject
    private LanguageBean languageBean;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return s;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        LocalDateTime date = (LocalDateTime) o;
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                .withLocale(languageBean.getLocale());
        return formatter.format(date);
    }
}
