package de.fhg.fokus.edp.mqa.service.duplicates;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.*;


/**
 * 
 * This class provides a functionality to assess similarities
 * of titles and descriptions of a large number of datasets.
 *
 * For reasons of speed, a modified form of TLSH is used,
 * a locality-sensitive string hashing function
 * which generates brief fingerprints
 * designed to permit a fast distance calculation.
 *
 * In order to work, this class requires one or more text file(s)
 * which contain URIs, text lengths, and the fingerprints of the texts.
 * These files are generated from the EDP data by another program.
 * It seems to be reasonable to have one such file per catalog,
 * or one file per language area (with a few catalogs each).
 *
 * The paths of these files are passed to the constructor of this class,
 * which reads the files and sets up an internal data structure.
 * Creating more than one instance of this class is probably not useful.
 * After creation, the class instance can be queried
 * via the function getSimilars.
 *
 * @author sca
 */
@ApplicationScoped
public class SimilarityService {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.FP_REMOTE_URL)
    private String fingerprintRemoteUrl;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.FP_DIRECTORY)
    private String fingerprintFileDir;


    /**
     * Each byte of a TLSH fingerprint contain 4 2-bit values.
     * The difference between two such bytes x,y
     * is rapidly determined by diffCount[(x^y)&255].
     * Initialization in constructor; no alterations afterwards.
     */
    private int[] diffCount = new int[256];

    /**
     * Assigns TLSH fingerprint and length of title+description to every URI.
     * Initialization in constructor; no alterations afterwards.
     */
    private HashMap<String, WithIntAttr<byte[]>> fingerprint = new HashMap<>();

    private final int unitDist = 8;

    public void initAllLanguages(@Observes @Initialized(ApplicationScoped.class) Object init) {

        LOG.info("Fingerprinted URIs are loaded from {}", fingerprintFileDir);

        // initialize diffCount array:
        diffCount[0] = 0;
        int length = 1;
        do {
            for (int i = 0; i < length; i++) {
                //differentiate a bit:
                diffCount[i + length] = diffCount[i + 3 * length] = diffCount[i] + unitDist;
                diffCount[i + 2 * length] = diffCount[i] + unitDist + 2;
            }
            length *= 4;
        } while (length < 256);

        // read work data from file(s):
        File[] filesToFingerprint = new File(fingerprintFileDir).listFiles();

        if (filesToFingerprint == null) {
            LOG.error("Could not retrieve list of files with fingerprints at path specified: {}", fingerprintFileDir);
        } else if (filesToFingerprint.length == 0) {
            LOG.error("No files with fingerprints found at path specified: {}", fingerprintFileDir);
        } else {
            Arrays.stream(filesToFingerprint).forEach(this::initSingleLanguage);
            LOG.info("Loaded [{}] FP dumps from disk", filesToFingerprint.length);
        }
    }

    void initSingleLanguage(File langFile) {
        try (InputStreamReader sr = new InputStreamReader(new FileInputStream(langFile))) {
            StreamTokenizer tok = new StreamTokenizer(sr);
            tok.quoteChar('"');
            tok.parseNumbers();    //??
            tok.eolIsSignificant(false);

            while (tok.nextToken() != StreamTokenizer.TT_EOF) {
                if (tok.ttype != '"') {
                    LOG.error("URI string expected in file [{}]", langFile.getAbsolutePath());
                    break;
                }

                String uri = tok.sval;
                tok.nextToken();

                if (tok.ttype != '"') {
                    LOG.error("Fingerprint string expected in file [{}]", langFile.getAbsolutePath());
                    break;
                }

                if (tok.sval.length() != 32) {
                    LOG.error("32 hex digits expected in file [{}]", langFile.getAbsolutePath());
                    break;
                }

                byte[] bytes = DatatypeConverter.parseHexBinary(tok.sval);

                if (tok.nextToken() != StreamTokenizer.TT_NUMBER) {
                    LOG.error("Number expected in file [{}]", langFile.getAbsolutePath());
                    break;
                }

                int textLength = (int) tok.nval;

                fingerprint.put(uri, new WithIntAttr<>(bytes, textLength));
            }
            LOG.debug("Successfully (re)loaded file [{}]", langFile.getAbsolutePath());
        } catch (IOException e) {
            LOG.error("Failed to read File [{}]", langFile.getAbsolutePath(), e);
        }
    }

	/**
	 * Compare title+description of one dataset in EDP with all others.
	 * 
	 * @param puri URI of dataset to be compared.
     * @param limit the maximum number of results to be returned. Passing 0 will return all results.
	 * @return Json array with hits up to distance 40, in ascending order.
	 */
    public String getSimilars(String puri, int limit) {
        WithIntAttr<byte[]> comp = fingerprint.get(puri);

        if (limit == 0)
            limit = fingerprint.size();

        if (comp != null) {

            byte[] compLeft = comp.getVal();
            ArrayList<WithIntAttr<String>> listRes = new ArrayList<>();

            // Compare <puri> against all other datasets,
            // collecting results in ArrayList listRes.
            // (following loop needs to be fast):
            for (Map.Entry<String, WithIntAttr<byte[]>> curr : fingerprint.entrySet()) {
                byte[] compRight = curr.getValue().getVal();
                int distance
                        = diffCount[(compLeft[0] ^ compRight[0]) & 255]
                        + diffCount[(compLeft[1] ^ compRight[1]) & 255]
                        + diffCount[(compLeft[2] ^ compRight[2]) & 255]
                        + diffCount[(compLeft[3] ^ compRight[3]) & 255]
                        + diffCount[(compLeft[4] ^ compRight[4]) & 255]
                        + diffCount[(compLeft[5] ^ compRight[5]) & 255]
                        + diffCount[(compLeft[6] ^ compRight[6]) & 255]
                        + diffCount[(compLeft[7] ^ compRight[7]) & 255]
                        + diffCount[(compLeft[8] ^ compRight[8]) & 255]
                        + diffCount[(compLeft[9] ^ compRight[9]) & 255]
                        + diffCount[(compLeft[10] ^ compRight[10]) & 255]
                        + diffCount[(compLeft[11] ^ compRight[11]) & 255]
                        + diffCount[(compLeft[12] ^ compRight[12]) & 255]
                        + diffCount[(compLeft[13] ^ compRight[13]) & 255]
                        + diffCount[(compLeft[14] ^ compRight[14]) & 255]
                        + diffCount[(compLeft[15] ^ compRight[15]) & 255];
                distance /= unitDist;    // cf. diffCount initialization in constructor

                // incorporate length comparison (because basic TLSH fingerprinting is length-agnostic):
                double lngLeft = comp.getAttr(), lngRight = curr.getValue().getAttr();
                double x = Math.abs(lngLeft - lngRight) / Math.max(lngLeft, lngRight);

                // now x is between 0.0 and 1.0 inclusively; polynomial weighting follows:
                distance += (int) (48.0 * x * x * (-2.0 * x + 3.0));
                if (distance <= 40 && !curr.getKey().equals(puri)) // ... compare against all *other* datasets ...
                    listRes.add(new WithIntAttr<>(curr.getKey(), distance));
            }

            // sort results in ascending distance:
            Collections.sort(listRes);

            // return Json result built from listRes:
            JsonArrayBuilder jab = Json.createArrayBuilder();

            // only return list of IDs instead of entire URI
            listRes.stream().limit(limit).forEach(curr -> {
                String id = StringUtils.substringAfterLast(curr.getVal(), "/");
                jab.add(Json.createObjectBuilder()
                        .add("uri", fingerprintRemoteUrl + "/data/dataset/" + id)
                        .add("id", id)
                        .add("dist", curr.getAttr())
                        .build());
            });

            return jab.build().toString();
        } else {
            LOG.error("Could not find fingerprint for URI " + puri);
            return null;
        }
    }

    /**
     * One-parameter generic class with two immutable fields,
     * one of the generic parameter type,
     * the other one of type integer.
     */
    private class WithIntAttr<T> implements Comparable<WithIntAttr<T>> {
        private T val;
        private int attr;

        WithIntAttr(T pk, int pa) {
            val = pk;
            attr = pa;
        }

        T getVal() { return val; }
        int getAttr() { return attr; }

        @Override
        public int compareTo(WithIntAttr<T> other) { return attr - ((other).getAttr()); }
    }
}
