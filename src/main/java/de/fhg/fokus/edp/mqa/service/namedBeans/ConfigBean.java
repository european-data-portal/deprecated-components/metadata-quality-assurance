package de.fhg.fokus.edp.mqa.service.namedBeans;

import de.fhg.fokus.edp.mqa.service.config.SystemConfiguration;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import static de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys.MAINTENANCE;
import static de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys.URL_CHECKER_CONNECTION_TIMEOUT;

/**
 * Created by bdi on 08/12/15.
 */
@Named
@ApplicationScoped
public class ConfigBean {

    @Inject
    private SystemConfiguration conf;

    private int httpDistributionCheckTimeout;
    private boolean maintenance;

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        httpDistributionCheckTimeout = Integer.parseInt(conf.getProperties().getProperty(URL_CHECKER_CONNECTION_TIMEOUT));
        maintenance = Boolean.valueOf(conf.getProperties().getProperty(MAINTENANCE));
    }

    /**
     * Gets http distribution check timeout.
     *
     * @return the http distribution check timeout
     */
    public int getHttpDistributionCheckTimeout() {
        return httpDistributionCheckTimeout;
    }

    /**
     * Sets http distribution check timeout.
     *
     * @param httpDistributionCheckTimeout the http distribution check timeout
     */
    public void setHttpDistributionCheckTimeout(int httpDistributionCheckTimeout) {
        this.httpDistributionCheckTimeout = httpDistributionCheckTimeout;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public void setMaintenance(boolean maintenance) {
        this.maintenance = maintenance;
    }
}
