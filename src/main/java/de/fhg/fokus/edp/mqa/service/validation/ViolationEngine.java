package de.fhg.fokus.edp.mqa.service.validation;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by bdi on 02/06/15.
 */
@Qualifier
@Retention(RUNTIME)
@Target({FIELD, TYPE, METHOD})
public @interface ViolationEngine {
    /**
     * Value violation parser type.
     *
     * @return the violation parser type
     */
    ViolationParserType value();
}
