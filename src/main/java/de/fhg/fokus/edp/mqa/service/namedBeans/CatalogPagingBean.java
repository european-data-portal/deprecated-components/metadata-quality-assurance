package de.fhg.fokus.edp.mqa.service.namedBeans;

import com.google.common.primitives.Ints;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class CatalogPagingBean {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    private int totalRows;
    private int firstRow;
    private int rowsPerPage;
    private int totalPages;
    private int pageRange;
    private Integer[] pages;
    private int currentPage;


    private List<Catalog> catalogs;


    @PostConstruct
    public void init() {
        rowsPerPage = 10;
        pageRange = 6;

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if (params.get("page") != null) {
            try {
                firstRow = (Integer.valueOf(params.get("page")) - 1) * rowsPerPage;
                loadCatalogs();
            } catch (NumberFormatException e) {
                LOG.error("Error parsing page param value [{}]", params.get("page"));
            }
        }
    }

    private void loadCatalogs() {
        List<Catalog> allCatalogs = vdc.listAllCatalogs();
        allCatalogs.sort(new CatalogTitleComparator());

        catalogs = allCatalogs.subList(firstRow, firstRow + rowsPerPage < allCatalogs.size() ? firstRow + rowsPerPage : allCatalogs.size());
        totalRows = Ints.checkedCast(vdc.countAllCatalogs());

        // Set currentPage, totalPages and pages.
        currentPage = (totalRows / rowsPerPage) - ((totalRows - firstRow) / rowsPerPage) + 1;
        totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
        int pagesLength = Math.min(pageRange, totalPages);
        pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
    }

    public List<Catalog> getCatalogs() {
        loadCatalogs();
        return catalogs;
    }

    public void setCatalogs(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }

    /**
     * Gets total rows.
     *
     * @return the total rows
     */
    public int getTotalRows() {
        return totalRows;
    }

    /**
     * Sets total rows.
     *
     * @param totalRows the total rows
     */
    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    /**
     * Gets first row.
     *
     * @return the first row
     */
    public int getFirstRow() {
        return firstRow;
    }

    /**
     * Sets first row.
     *
     * @param firstRow the first row
     */
    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    /**
     * Gets rows per page.
     *
     * @return the rows per page
     */
    public int getRowsPerPage() {
        return rowsPerPage;
    }

    /**
     * Sets rows per page.
     *
     * @param rowsPerPage the rows per page
     */
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    /**
     * Gets total pages.
     *
     * @return the total pages
     */
    public int getTotalPages() {
        return totalPages;
    }

    /**
     * Sets total pages.
     *
     * @param totalPages the total pages
     */
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Get pages integer [ ].
     *
     * @return the integer [ ]
     */
    public Integer[] getPages() {
        return pages;
    }

    /**
     * Sets pages.
     *
     * @param pages the pages
     */
    public void setPages(Integer[] pages) {
        this.pages = pages;
    }

    /**
     * Gets current page.
     *
     * @return the current page
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    private class CatalogTitleComparator implements Comparator<Catalog> {
        public int compare(Catalog c1, Catalog c2) {
            return c1.getName().trim().toLowerCase().compareTo(c2.getName().trim().toLowerCase());
        }
    }
}
