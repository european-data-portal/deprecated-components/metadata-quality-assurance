package de.fhg.fokus.edp.mqa.service.ckan;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.config.SystemConfiguration;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClient;
import de.fhg.fokus.edp.mqa.service.fetch.MqaHttpProvider;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.model.Licence;
import de.fhg.fokus.edp.mqa.service.rest.Authenticator;
import de.fhg.fokus.edp.mqa.service.utils.Formats;
import de.fhg.fokus.edp.mqa.service.utils.MapperUtils;
import de.fhg.fokus.edp.mqa.service.validation.*;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static de.fhg.fokus.edp.mqa.service.config.Constants.*;
import static de.fhg.fokus.edp.mqa.service.validation.ViolationParserType.JSON_SCHEMA;

/**
 * Created by bdi on 24/04/15.
 */
@Dependent
public class DcatapDataClientCkanImpl implements Serializable, DcatapDataClient {

    @Inject
    private MapperUtils mapperUtils;

    private CkanClientAction ckanClientAction;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    private MqaHttpProvider httpProvider;

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.CKAN_CHECK_URL)
    private String ckanUrl;

    @Inject
    @ViolationEngine(JSON_SCHEMA)
    private ViolationParser violationParser;

    @Inject
    private Authenticator authenticator;

    @Inject
    private SystemConfiguration conf;

    @Inject
    private Formats formats;

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        authenticator.setUsername(conf.getProperties().getProperty(MqaServiceConfigKeys.CKAN_BASICAUTH_USERNAME));
        authenticator.setPassword(conf.getProperties().getProperty(MqaServiceConfigKeys.CKAN_BASICAUTH_PASSWORD));
        ResteasyClient client = new ResteasyClientBuilder().build().register(authenticator);
        ResteasyWebTarget target = client.target(ckanUrl);
        ckanClientAction = target.proxy(CkanClientAction.class);
    }

    @Override
    public List getCatalogs() {

        JsonNode olResponse = mapperUtils.convertToJsonNode(ckanClientAction.organizationList().readEntity(String.class));

        List<Catalog> catalogs = new ArrayList<>();

        // Iterate through each organization via their ID
        olResponse.get("result").forEach(jsonNode -> {
            ObjectNode body = new ObjectNode(JsonNodeFactory.instance);

            body.put("id", jsonNode.textValue());
            //body.put("include_datasets", "true");

            // Get content of the organization
            Response organisationShowResponse = ckanClientAction.organizationShow(body.toString());

            // Get catalog information
            JsonNode jsonNodeCatalog = mapperUtils.convertToJsonNode(organisationShowResponse.readEntity(String.class)).get("result");

            // Create new empty catalog
            Catalog jsonCatalog = vdc.createCatalog();
            jsonCatalog.setName(jsonNodeCatalog.get(CATALOG_NAME).textValue());
            jsonCatalog.setTitle(jsonNodeCatalog.get(CATALOG_TITLE).textValue());
            jsonCatalog.setInstanceId(jsonNodeCatalog.get(CKAN_OBJECT_INSTANCE_ID).textValue());
            jsonCatalog.setDescription(jsonNodeCatalog.get(CATALOG_DESCRIPTION).textValue());

            // spatial is array
            JsonNode spatial = jsonNodeCatalog.get(CATALOG_SPATIAL);
            if (spatial != null && spatial.get(0) != null)
                jsonCatalog.setSpatial(spatial.get(0).textValue());


            // FIXME: Commented due to CKAN bug
//            jsonCatalog.setModified(LocalDateTime.ofInstant(DateUtils.convert(jsonNodeCatalog.get(Constants.CATALOG_MODIFIED)
//                    .getTextValue()).toInstant(), ZoneId.systemDefault()));
            // TODO: Solve Owner Issue...
//            Publisher publisher = new PublisherEntity();
//            publisher.setName(jsonNodeCatalog.get(Constants.CATALOG_PUBLISHER).get(Constants
//                    .PUBLISHER_NAME).getTextValue());
//            publisher.setEmail(jsonNodeCatalog.get(Constants.CATALOG_PUBLISHER).get(Constants
//                    .PUBLISHER_EMAIL).getTextValue());

            // Update catalog if it already exists
            Catalog catalog = vdc.getCatalogByName(jsonCatalog.getName());
            if (catalog != null) {
                LOG.trace("Catalog [{}] already exists -> update", jsonCatalog.getName());
                catalog.setName(jsonCatalog.getName());
                catalog.setTitle(jsonCatalog.getTitle());
                catalog.setSpatial(jsonCatalog.getSpatial());
                catalog.setDescription(jsonCatalog.getDescription());
                //catalog.setPublisher(jsonCatalog.getPublisher());
                catalog = vdc.updateEntity(catalog);
                catalogs.add(catalog);
            } else {
                // Persist new catalog
                catalogs.add(vdc.persistEntity(jsonCatalog));
            }
        });

        return catalogs;
    }

    @Override
    public List<Dataset> getDatasetsByCatalog(String catalogName, int limit, int page) {
        Response packages = ckanClientAction.packageSearch("organization:" + catalogName, limit, page);
        JsonNode results = mapperUtils.getSearchResult(mapperUtils.convertToJsonNode(mapperUtils.extractJsonFromResponse(packages)));
        return convertDatasets(results);
    }

    private List<Dataset> convertDatasets(JsonNode jsonDatasets) {
        List<Dataset> datasets = new ArrayList<>();
        for (JsonNode jsonNode : jsonDatasets) {
            Dataset dataset = vdc.createDataset();
            dataset.setName(jsonNode.get(DATASET_NAME).textValue());
            dataset.setTitle(jsonNode.get(DATASET_TITLE).textValue());
            dataset.setInstanceId(jsonNode.get(DATASET_ID).textValue());
            dataset.setLicenceId(jsonNode.get(DATASET_LICENCE_ID).textValue());

            try {
                dataset.getViolations().addAll(violationParser.validate(JsonLoader.fromString(jsonNode.toString())));
            } catch (IOException e) {
                LOG.error("Cannot convert JsonNode {} to String", jsonNode.toString());
            }

            // Iterate through each distribution that comes from the current json dataset
            for (JsonNode distributionNode : jsonNode.get(DATASET_DISTRIBUTIONS)) {
                Distribution distribution = vdc.createDistribution();

                try {
                    for (JsonNode accessUrlNode : distributionNode.get(DISTRIBUTION_ACCESS_URL)) {
                        distribution.setAccessUrl(accessUrlNode.textValue());
                        break; // TODO: Handle multiple access urls
                    }
                } catch (NullPointerException e) {
                    LOG.trace("Dataset [{}] has no access_url", distribution
                            .getAccessUrl(), dataset.getName());
                }

                distribution.setInstanceId(distributionNode.get(CKAN_OBJECT_INSTANCE_ID).textValue());
                distribution.setFormat(distributionNode.get(DISTRIBUTION_FORMAT).textValue());
                distribution.setMachineReadable(formats.isFormatMachineReadable(distribution.getFormat()));
                distribution.setMediaType(distributionNode.get(DISTRIBUTION_MIMETYPE).textValue());

                try {
                    for (JsonNode downloadUrlNode : distributionNode.get(DISTRIBUTION_DOWNLOAD_URL)) {
                        distribution.setDownloadUrl(downloadUrlNode.textValue());
                        break; // TODO: Handle multiple download urls
                    }
                } catch (NullPointerException e) {
                    LOG.trace("Distribution with access_url [{}] of dataset [{}] has no download_url", distribution
                            .getAccessUrl(), dataset.getName());
                }
                dataset.getDistributions().add(distribution);
            }

            datasets.add(dataset);

        }
        return datasets;
    }

    @Override
    public void refreshKnownLicences() {
        try {
            httpProvider.getKnownLicences().forEach(jsonNode -> {
                Licence l = convertJsonToLicence((JsonObject) jsonNode);

                // store licence in db
                if (vdc.getLicenceById(l.getLicenceId()) == null) {
                    vdc.persistEntity(l);
                } else {
                    //FIXME the line below simply adds duplicates
                    //knownLicences.add(vdc.updateEntity(l));
                }
            });
        } catch (IOException e) {
            LOG.error("Failed to refresh licences: {}", e);
        }
    }

    private Licence convertJsonToLicence(JsonObject jsonNode) {
        Licence l = vdc.createLicence();

        l.setLicenceId(jsonNode.getString(LICENCE_ID));
        l.setStatus(jsonNode.getString(LICENCE_STATUS));
        l.setMaintainer(jsonNode.getString(LICENCE_MAINTAINER));
        l.setOdConformance(jsonNode.getString(LICENCE_OD_CONFORMANCE));
        l.setFamily(jsonNode.getString(LICENCE_FAMILY));
        l.setTitle(jsonNode.getString(LICENCE_TITLE));
        l.setUrl(jsonNode.getString(LICENCE_URL));
        l.setOsdConformance(jsonNode.getString(LICENCE_OSD_CONFORMANCE));

        l.setDomainData(jsonNode.getBoolean(LICENCE_DOMAIN_DATA));
        l.setDomainContent(jsonNode.getBoolean(LICENCE_DOMAIN_CONTENT));
        l.setDomainSoftware(jsonNode.getBoolean(LICENCE_DOMAIN_SOFTWARE));

//        l.setOkdCompliant(jsonNode.getBoolean(LICENCE_OKD_COMPLIANCE));
//        l.setOsiCompliant(jsonNode.getBoolean(LICENCE_OSI_COMPLIANCE));
        l.setGeneric(jsonNode.getBoolean(LICENCE_IS_GENERIC));

        l.setDbUpdate(LocalDateTime.now());

        return l;
    }
}
