package de.fhg.fokus.edp.mqa.service.fetch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.config.SystemConfiguration;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapData;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClient;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClientType;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by bdi on 23/06/15.
 */
public class CatalogTask implements Callable<CatalogTaskResponse>, Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private SystemConfiguration conf;

    @Inject
    @DcatapData(DcatapDataClientType.CKAN)
    private DcatapDataClient dcatapDataClient;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.DCATAP_DATACLIENT_PAGE_LIMIT)
    private String dcatApDataClientPageLimit;

    @Inject
    private MqaHttpProvider httpProvider;

    @Inject
    private ProcessData processData;

    private String catalogName;

    @Override
    public CatalogTaskResponse call() {

        List<Dataset> remoteDatasets;

        Instant start = Instant.now();
        int pageCount = 0;

        // stats
        AtomicInteger newDatasetsCount = new AtomicInteger(0);
        AtomicInteger updatedDatasetsCount = new AtomicInteger(0);

        final Stopwatch stopwatchTotalTime = Stopwatch.createStarted();

        do {
            remoteDatasets = dcatapDataClient.getDatasetsByCatalog(catalogName, Integer.parseInt(dcatApDataClientPageLimit), pageCount);

            // Validating and persisting
            ProcessData.ProcessDataResponse response = processData.processDatasets(catalogName, remoteDatasets);

            newDatasetsCount.addAndGet(response.getNewDatasetCount());
            updatedDatasetsCount.addAndGet(response.getUpdatedDatasetCount());

            for (Dataset dataset : response.getDatasets()) {
                for (Distribution distribution : dataset.getDistributions()) {
                    if (distribution.getAccessUrl() != null && !distribution.getAccessUrl().isEmpty()) {
                        try {
                            List<String> urls = new ArrayList<>();
                            urls.add(distribution.getAccessUrl());

                            if (distribution.getDownloadUrl() != null
                                    && !distribution.getDownloadUrl().isEmpty()
                                    && !distribution.getDownloadUrl().equals(distribution.getAccessUrl())) {

                                urls.add(distribution.getDownloadUrl());
                            }

                            String body = buildRequestJson(distribution.getInstanceId(), urls);
                            httpProvider.sendUrlCheckPost(String.valueOf(distribution.getId()), body);

                        } catch (JsonProcessingException e) {
                            LOG.warn("Error building URL check request body: {}", e.getMessage());
                        }
                    }
                }
            }

            pageCount = pageCount + Integer.parseInt(dcatApDataClientPageLimit);

        } while (!remoteDatasets.isEmpty());

        Integer datasetCount = newDatasetsCount.get() + updatedDatasetsCount.get();

        LOG.info("{} completed | {} DS | {} new | {} updated | {}s total",
                catalogName, datasetCount, newDatasetsCount.get(), updatedDatasetsCount.get(), stopwatchTotalTime.stop().elapsed(TimeUnit.SECONDS));

        return new CatalogTaskResponse(catalogName, datasetCount, Duration.between(start, Instant.now()));
    }

    private String buildRequestJson(String distributionInstanceId, List<String> urls) throws JsonProcessingException {
        String callback = StringUtils.removeEnd(conf.getProperties().getProperty(MqaServiceConfigKeys.HOST_URL), "/")
                + "/mqa-service/api/callback/" + distributionInstanceId;

        UrlCheckRequest request = new UrlCheckRequest();
        request.setUrls(urls);
        request.setCallback(callback);

        return new ObjectMapper().writeValueAsString(request);
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    private class UrlCheckRequest {
        private List<String> urls;
        private String callback;

        public List<String> getUrls() {
            return urls;
        }

        public void setUrls(List<String> urls) {
            this.urls = urls;
        }

        public String getCallback() {
            return callback;
        }

        public void setCallback(String callback) {
            this.callback = callback;
        }
    }
}
