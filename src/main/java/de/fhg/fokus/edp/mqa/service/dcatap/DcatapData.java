package de.fhg.fokus.edp.mqa.service.dcatap;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by bdi on 24/04/15.
 */
@Qualifier
@Retention(RUNTIME)
@Target({FIELD, TYPE, METHOD})
public @interface DcatapData {

    /**
     * Value dcatap data client type.
     *
     * @return the dcatap data client type
     */
    DcatapDataClientType value();

}
