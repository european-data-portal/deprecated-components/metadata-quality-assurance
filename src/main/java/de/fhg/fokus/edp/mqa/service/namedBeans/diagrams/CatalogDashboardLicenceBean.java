package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by fritz on 28.10.16.
 */
@RequestScoped
@Named
public class CatalogDashboardLicenceBean extends CatalogDiagramBean implements Serializable {

    private Set<ReportElementBean> ratioKnownUnknownPieChart;
    private Set<ReportElementBean> mostUsedLicencesColumnChart;

    @Override
    public boolean renderSection() {
        return getRenderMetric("/metric/catalogues/" + catalogId + "/render/licences");
    }

    @Override
    void createCharts() {
        ratioKnownUnknownPieChart = getSetWithBinaryValues("/metric/catalogues/" + catalogId + "/datasets/known_licences");
        mostUsedLicencesColumnChart = getSetWithMultipleValuesSorted("/metric/catalogues/" + catalogId + "/datasets/licences", 5);
    }

    public Set<ReportElementBean> getRatioKnownUnknownPieChart() {
        return ratioKnownUnknownPieChart;
    }

    public void setRatioKnownUnknownPieChart(Set<ReportElementBean> ratioKnownUnknownPieChart) {
        this.ratioKnownUnknownPieChart = ratioKnownUnknownPieChart;
    }

    public Set<ReportElementBean> getMostUsedLicencesColumnChart() {
        return mostUsedLicencesColumnChart;
    }

    public void setMostUsedLicencesColumnChart(Set<ReportElementBean> mostUsedLicencesColumnChart) {
        this.mostUsedLicencesColumnChart = mostUsedLicencesColumnChart;
    }
}
