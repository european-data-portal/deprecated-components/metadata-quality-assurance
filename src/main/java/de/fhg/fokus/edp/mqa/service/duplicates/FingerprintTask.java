package de.fhg.fokus.edp.mqa.service.duplicates;

import de.fhg.fokus.edp.mqa.service.log.Log;
import org.quartz.Trigger;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.concurrent.Callable;

/**
 * Created by fritz on 13.03.17.
 */
public class FingerprintTask implements Callable<FingerprintTaskResponse>, Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    FingerprintGenerator fingerprintGenerator;

    private String langCode;

    private Trigger trigger;

    @Override
    public FingerprintTaskResponse call() {
        try {
            Path langFile = fingerprintGenerator.fingerprint(langCode.toUpperCase());
            return new FingerprintTaskResponse(langCode, trigger, langFile);
        } catch (LanguageFailedException e) {
            LOG.error("Fingerprinting language [{}] failed", langCode, e);
            return new FingerprintTaskResponse(langCode, trigger, null);
        }
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }
}
