package de.fhg.fokus.edp.mqa.service.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by bdi on 09.04.2015.
 */
@ApplicationScoped
public class SystemConfiguration {

    private Logger LOG = LoggerFactory.getLogger(this.getClass());

    private Properties props;

    // Show config only first time when loaded from application container
    @PostConstruct
    public void init() {

        loadProps();

        if (props != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Loaded MQA configuration...\n");
            for (String propertyName : props.stringPropertyNames()) {
                if (propertyName.contains("password") || propertyName.equals("admin.token")) {
                    sb.append(String.format("%-40s = %s\n", propertyName, "<secret>"));
                } else {
                    sb.append(String.format("%-40s = %s\n", propertyName, props.getProperty(propertyName)));
                }
            }
            LOG.info(sb.toString());
        }
    }

    private void loadProps() {
        try {
            Path configFile = Paths.get("/etc/mqa", "mqa-service.properties");
            InputStream in;
            props = new Properties();
            if (Files.exists(configFile)) {
                in = Files.newInputStream(configFile);
                //LOG.info("Loaded configuration file {}", configFile.toAbsolutePath());
            } else {
                in = this.getClass().getResourceAsStream("/mqa-service.properties");
                //LOG.info("Loaded default configuration file");
            }
            props.load(in);
        } catch (IOException e) {
            LOG.error("Cannot load properties file", e);
        }
    }

    public Properties getProperties() {
        return props;
    }
}
