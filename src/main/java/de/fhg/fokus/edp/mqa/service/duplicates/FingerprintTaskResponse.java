package de.fhg.fokus.edp.mqa.service.duplicates;

import org.quartz.Trigger;

import java.nio.file.Path;

/**
 * Created by fritz on 17.03.17.
 */
public class FingerprintTaskResponse {

    private String langCode;

    private Trigger trigger;

    private Path fileName;

    public FingerprintTaskResponse(String langCode, Trigger trigger, Path fileName) {
        this.langCode = langCode;
        this.trigger = trigger;
        this.fileName = fileName;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public Path getFileName() {
        return fileName;
    }

    public void setFileName(Path fileName) {
        this.fileName = fileName;
    }
}
