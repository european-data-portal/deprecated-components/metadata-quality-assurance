package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.namedBeans.CurrentCatalogBean;
import org.jboss.weld.exceptions.WeldException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Locale;

/**
 * Created by fritz on 03.11.16.
 */
abstract class CatalogDiagramBean extends DiagramBean {

    String catalogId;

    @Inject
    private CurrentCatalogBean currentCatalog;

    @PostConstruct
    public void init() {
        try {
            catalogId = currentCatalog.getCatalogId();
        } catch (WeldException e) {
            LOG.info("Just fine if this happened during deployment: {}", e.getMessage());
        }

        isSaveToLoadDiagrams = catalogId != null;

        super.init();
    }

    public void fillWithSpecificCatalog(String catalogId, Locale locale) {
        this.catalogId = catalogId;
        loadDiagramsWithLocale(locale);
    }
}
