package de.fhg.fokus.edp.mqa.service.dcatap;

/**
 * Created by bdi on 24/04/15.
 */
public enum DcatapDataClientType {
    /**
     * Ckan dcatap data client type.
     */
    CKAN
}
