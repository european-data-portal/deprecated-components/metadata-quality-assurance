package de.fhg.fokus.edp.mqa.service.model;

import java.time.LocalDateTime;

/**
 * Created by fritz on 05.10.16.
 */
public interface Licence {
    long getId();

    void setId(long id);

    String getStatus();

    void setStatus(String status);

    String getMaintainer();

    void setMaintainer(String maintainer);

    String getOdConformance();

    void setOdConformance(String odConformance);

    String getFamily();

    void setFamily(String family);

    String getTitle();

    void setTitle(String title);

    String getUrl();

    void setUrl(String url);

    String getOsdConformance();

    void setOsdConformance(String osdConformance);

    boolean isDomainData();

    void setDomainData(boolean domainData);

    boolean isDomainContent();

    void setDomainContent(boolean domainContent);

    boolean isOkdCompliant();

    void setOkdCompliant(boolean okdCompliant);

    boolean isOsiCompliant();

    void setOsiCompliant(boolean osiCompliant);

    boolean isGeneric();

    void setGeneric(boolean generic);

    boolean isDomainSoftware();

    void setDomainSoftware(boolean domainSoftware);

    String getLicenceId();

    void setLicenceId(String licenceId);

    LocalDateTime getDbUpdate();

    void setDbUpdate(LocalDateTime dbUpdate);
}
