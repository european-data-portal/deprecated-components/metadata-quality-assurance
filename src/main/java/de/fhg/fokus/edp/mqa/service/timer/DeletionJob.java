package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;

import javax.inject.Inject;

/**
 * Created by fritz on 10.05.17.
 */
public class DeletionJob implements InterruptableJob {

    private Thread thread;

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    private MqaScheduler mqaScheduler;

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        thread.interrupt();
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        thread = Thread.currentThread();

        try {
            LOG.info("Starting deletion phase...");
            if (mqaScheduler.getPreviousValidationRunDate() != null) {
                vdc.removeOutdatedEntities(mqaScheduler.getPreviousValidationRunDate());
                LOG.info("Deletion phase finished successfully");
            } else {
                LOG.error("Deletion phase encountered an error. No entities were removed.");
            }
        } catch (Exception e) {
            LOG.error("Error during deletion phase", e);
        }
    }
}
