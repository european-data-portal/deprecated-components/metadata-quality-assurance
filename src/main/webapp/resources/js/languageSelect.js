/**
 * Created by lli on 24.03.2017.
 *
 *
 */
var language = 'en';
$(document).ready(function () {
    var myRe = new RegExp('/.{2}/', 'g');
    var myArray = myRe.exec(window.location.href + "/");
    language = myArray[0].substring(1, 3);
    document.getElementById('language-selector').value = language;
    init();
});
var init = function () {
    jQuery.i18n.properties({
            name: 'indextranslations',
            path: window.location.origin + "/mqa-service/resources/languages/",
            mode: 'both',
            language: language,
            checkAvailableLanguages: false,
            async: false,
            callback: function () {
                var translationId = 70;
                var count;
                for (count = translationId; count >= 0; count--) {
                    if (document.getElementById('text' + count) == null) {
                        continue;
                    }
                    if (count === 54) {
                        document.getElementById('text' + count).value = jQuery.i18n.prop(document.getElementById('text' + count).title);
                    } else if (count === 9) {
                        document.getElementById('text' + count).placeholder = jQuery.i18n.prop(document.getElementById('text' + count).title);
                    } else
                        document.getElementById('text' + count).innerHTML = jQuery.i18n.prop(document.getElementById('text' + count).title);
                }

                var languageSelect = document.getElementsByClassName('languageSelect');

                for (var count1 = languageSelect.length - 1; count1 >= 0; count1--) {
                    if (languageSelect.item(count1) == null || languageSelect.item(count1).href == undefined) {
                        continue;
                    }
                    languageSelect.item(count1).href = languageSelect.item(count1).href.replace(/en/i, language);
                }
            }
        }
    );
};

var changelanguage = function (value) {
    var checkLast = window.location.href + "/";
    console.log(checkLast);
    console.log(checkLast.lastIndexOf(value));
    checkLast = checkLast.replace(/\/.{2}\//, "/" + value + "/");
    checkLast = checkLast.slice(0, -1);
    window.location = checkLast;
    language = value;
};

var goSearch = function () {
    window.location = window.location.origin + "/" + language + "/search/site/" + document.getElementById('text9').value;
};