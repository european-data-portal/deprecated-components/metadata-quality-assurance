package de.fhg.fokus.edp.mqa.service.test.util;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;

import java.io.File;

/**
 * Created by bdi on 05/08/15.
 */
public class ArquillianUtil {

    /**
     * Create web archive web archive.
     *
     * @return the web archive
     */
    public static WebArchive createWebArchive() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "mqa-service-test.war");
        war.addPackage("de.fhg.fokus.edp.mqa.service.ckan");
        war.addPackage("de.fhg.fokus.edp.mqa.service.config");
        war.addPackage("de.fhg.fokus.edp.mqa.service.dcatap");
        war.addPackage("de.fhg.fokus.edp.mqa.service.model");
        war.addPackage("de.fhg.fokus.edp.mqa.service.persistence");
        war.addPackage("de.fhg.fokus.edp.mqa.service.persistence.entity");
        war.addPackage("de.fhg.fokus.edp.mqa.service.timer");
        war.addPackage("de.fhg.fokus.edp.mqa.service.utils");
        war.addPackage("de.fhg.fokus.edp.mqa.service.validation");
        war.addPackage("de.fhg.fokus.edp.mqa.service.fetch");
        war.addPackage("de.fhg.fokus.edp.mqa.service.log");
        war.addPackage("de.fhg.fokus.edp.mqa.service.meta");
        war.addPackage("de.fhg.fokus.edp.mqa.service.similarities");
        war.addPackage("de.fhg.fokus.edp.mqa.service.namedBeans");
        war.addPackage("de.fhg.fokus.edp.mqa.service.cdi");
        war.addPackage("de.fhg.fokus.edp.mqa.service.rest");
        war.addPackage("de.fhg.fokus.edp.mqa.service.harvester");
        war.addPackage("de.fhg.fokus.edp.mqa.service.duplicates");
        war.addClass(EntityUtils.class);
        war.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        war.addAsResource("test-persistence.xml", "META-INF/persistence.xml");
        war.addAsResource("test-mqa-service.properties", "mqa-service.properties");
        war.setWebXML(new File("src/main/webapp/WEB-INF/web.xml"));
        war.addAsWebResource(new File("src/main/webapp/WEB-INF/faces-config.xml"), "faces-config.xml");
        war.addAsWebInfResource("wildfly-ds.xml");

        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml");

        File[] jackson = pom.resolve("org.jboss.resteasy:resteasy-jackson-provider").withTransitivity().asFile();
        war.addAsLibraries(jackson);

        File[] restClient = pom.resolve("org.jboss.resteasy:resteasy-client").withTransitivity().asFile();
        war.addAsLibraries(restClient);

        File[] jsonSchema = pom.resolve("com.github.fge:json-schema-validator").withTransitivity().asFile();
        war.addAsLibraries(jsonSchema);

        File[] commonsLang = pom.resolve("org.apache.commons:commons-lang3").withTransitivity().asFile();
        war.addAsLibraries(commonsLang);

        File[] rewriteServlet = pom.resolve("org.ocpsoft.rewrite:rewrite-servlet").withTransitivity().asFile();
        war.addAsLibraries(rewriteServlet);

        File[] beanUtils = pom.resolve("commons-beanutils:commons-beanutils").withTransitivity().asFile();
        war.addAsLibraries(beanUtils);

        File[] rewriteCdi = pom.resolve("org.ocpsoft.rewrite:rewrite-integration-faces").withTransitivity().asFile();
        war.addAsLibraries(rewriteCdi);

        File[] quartz = pom.resolve("org.quartz-scheduler:quartz").withoutTransitivity().asFile();
        war.addAsLibraries(quartz);

        File[] json = pom.resolve("org.json:json").withoutTransitivity().asFile();
        war.addAsLibraries(json);

        return war;
    }
}
