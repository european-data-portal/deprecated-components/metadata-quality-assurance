package de.fhg.fokus.edp.mqa.service.test;

import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.model.Licence;
import de.fhg.fokus.edp.mqa.service.test.util.ArquillianUtil;
import de.fhg.fokus.edp.mqa.service.test.util.EntityUtils;
import de.fhg.fokus.edp.mqa.service.test.util.TestConstants;
import de.fhg.fokus.edp.mqa.service.utils.Formats;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.apache.commons.lang3.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static de.fhg.fokus.edp.mqa.service.test.util.TestConstants.*;

/**
 * Created by bdi on 21/04/15.
 */
public class CreateUpdateSearchTest extends Arquillian {

    private static Logger LOG = LoggerFactory.getLogger(CreateUpdateSearchTest.class);

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    private EntityUtils utils;

    @Inject
    private Formats formats;

    /**
     * Create deployment web archive.
     *
     * @return the web archive
     */
    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ArquillianUtil.createWebArchive();
        LOG.info(war.toString(true));
        return war;
    }

    /**
     * Init.
     */
    @Test
    public void init() {
        Assert.assertNotNull(utils);
        utils.init();
    }

    /**
     * Find dataset with not accessible distribution.
     */
    @Test(dependsOnMethods = {"init"})
    public void findDatasetWithNotAccessibleDistributionTest() {
        List<Dataset> datasets = vdc.listDatasetsWithStatusCodesNotOkByCatalogName(0, 5, CATALOG_NAME_NOT_OK);
        Assert.assertEquals(datasets.size(), 1);
        Assert.assertEquals(datasets.get(0).getName(), vdc.getDatasetByName(TestConstants.DATASET_NOT_OK).getName());

    }

    /**
     * Find datasets with violations by catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void findDatasetsWithViolationsByCatalogTest() {
        List<Dataset> datasets = vdc.listDatasetsNonConformByCatalogName(0, 5, CATALOG_NAME_NOT_OK);
        Assert.assertEquals(datasets.size(), 1);
        Assert.assertEquals(datasets.get(0).getName(), vdc.getDatasetByName(TestConstants.DATASET_NOT_OK).getName());
    }

    /**
     * Gets non conformant datasets.
     */
    @Test(dependsOnMethods = {"init"})
    public void getNonConformantDatasetsTest() {
        Assert.assertEquals(vdc.countDatasetsConform(false), 1);
    }

    /**
     * Dataset list distribution not ok by catalog name count.
     */
    @Test(dependsOnMethods = {"init"})
    public void datasetListDistributionNotOkByCatalogNameCountTest() {
        Assert.assertEquals(vdc.countDatasetsWithStatusCodesNotOkByCatalogName(CATALOG_NAME_NOT_OK), 1);
    }

    /**
     * Dataset list with violation by catalog count.
     */
    @Test(dependsOnMethods = {"init"})
    public void datasetListWithViolationByCatalogCountTest() {
        Assert.assertEquals(vdc.countDatasetsNonConformByCatalogName(CATALOG_NAME_NOT_OK), 1);
    }

    /**
     * List distributions.
     */
    @Test(dependsOnMethods = {"init"})
    public void listDistributionsTest() {
        Assert.assertEquals(vdc.listAllDistributionIdsWithPaging(0, 2).size(), 2);
    }

    /**
     * List datasets.
     */
    @Test(dependsOnMethods = {"init"})
    public void listDatasetsTest() {
        Assert.assertEquals(vdc.listAllDatasetNamesWithPaging(0, 2).size(), 2);
    }

    /**
     * List catalogs.
     */
    @Test(dependsOnMethods = {"init"})
    public void listCatalogsTest() {
        Assert.assertEquals(vdc.listAllCatalogNamesWithPaging(0, 2).size(), 2);
    }

    /**
     * Count distribution by catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void countDistributionByCatalogTest() {
        Assert.assertEquals(vdc.countDistributionsByCatalogName(CATALOG_NAME_NOT_OK), 7);
    }

    /**
     * Count datasets by catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void countDatasetsByCatalogTest() {
        Assert.assertEquals(vdc.countDatasetsByCatalog(vdc.getCatalogByName(CATALOG_NAME_NOT_OK)), 2);
    }

    /**
     * Count violations by catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void countViolationsByCatalogTest() {
        Assert.assertEquals(vdc.countViolationsByCatalog(vdc.getCatalogByName(CATALOG_NAME_NOT_OK)), 1);
    }

    /**
     * Count distributions ok by catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void countDistributionsOkByCatalogTest() {
        Map<Catalog, Long> map = vdc.listCatalogsWithAccessibleDistributions();
        Optional<Map.Entry<Catalog, Long>> catalogEntry = map.entrySet().stream().filter(catalog ->
                StringUtils.equals(catalog.getKey().getName(), CATALOG_NAME_NOT_OK)).findFirst();

        if (catalogEntry.isPresent()) {
            Assert.assertEquals((long) catalogEntry.get().getValue(), 4);
        } else {
            Assert.fail();
        }
    }

    /**
     * Count distribution download ok.
     */
    @Test(dependsOnMethods = {"init"})
    public void countDistributionDownloadOkTest() {
        Assert.assertEquals(vdc.countDistributionsWithUrlOkByUrlType(false), 5);
    }

    /**
     * Count distribution access ok.
     */
    @Test(dependsOnMethods = {"init"})
    public void countDistributionAccessOkTest() {
        Assert.assertEquals(vdc.countDistributionsWithUrlOkByUrlType(true), 5);
    }

    /**
     * Count violation by dataset name.
     */
    @Test(dependsOnMethods = {"init"})
    public void countViolationByDatasetNameTest() {
        Assert.assertEquals(vdc.listViolationsByDatasetName(TestConstants.DATASET_NOT_OK).size(), 1);
    }

    /**
     * Count violation by catalog name.
     */
    @Test(dependsOnMethods = {"init"})
    public void countViolationByCatalogNameTest() {
        Assert.assertEquals(vdc.listViolationsByCatalogName(CATALOG_NAME_NOT_OK).size(), 1);
    }

    /**
     * Find distributions by dataset name.
     */
    @Test(dependsOnMethods = {"init"})
    public void findDistributionsByDatasetNameTest() {
        Assert.assertEquals(vdc.listDistributionsByDatasetName(TestConstants.DATASET_NOT_OK).size(), 4);
    }

    /**
     * Find dataset names by catalog name.
     */
    @Test(dependsOnMethods = {"init"})
    public void findDatasetNamesByCatalogNameTest() {
        List<String> names = new ArrayList<>();
        names.add(TestConstants.DATASET_NOT_OK);
        names.add(TestConstants.DATASET_OK);
        Assert.assertTrue(vdc.listDatasetNamesByCatalogName(CATALOG_NAME_NOT_OK).containsAll(names));
        Assert.assertEquals(vdc.listDatasetNamesByCatalogName(CATALOG_NAME_NOT_OK).size(), 2);
    }

    /**
     * Find catalog with non conformant datasets ordered test.
     */
    @Test(dependsOnMethods = {"init"})
    public void findCatalogWithNonConformantDatasetsOrderedTest() {
        Map<Catalog, Long> map = vdc.listAllCatalogsWithNonConformantDatasetCount();
        Optional<Map.Entry<Catalog, Long>> catalogEntry = map.entrySet().stream().filter(catalog ->
                StringUtils.equals(catalog.getKey().getName(), CATALOG_NAME_NOT_OK)).findFirst();

        if (catalogEntry.isPresent()) {
            Assert.assertEquals(catalogEntry.get().getValue().longValue(), 1);
        } else {
            Assert.fail();
        }
    }

    /**
     * Violation get all count.
     */
    @Test(dependsOnMethods = {"init"})
    public void violationGetAllCountTest() {
        Assert.assertEquals(vdc.countAllViolations(), 1);
    }

    /**
     * Formats available test.
     */
    @Test(dependsOnMethods = {"init"})
    public void formatsAvailableTest() {
        Assert.assertEquals(formats.getFormats().size(), 57);
    }

    /**
     * Is machine readable test.
     */
    @Test(dependsOnMethods = {"init"})
    public void isMachineReadableTest() {
        List<Distribution> distributions = vdc.listAllDistributions(0, 10);
        Assert.assertEquals(distributions.size(), 8);

        int count = 0;

        for (Distribution dist : distributions) {
            if (formats.isFormatMachineReadable(dist.getFormat())) {
                count++;
            }
        }

        Assert.assertEquals(count, 4);
    }

    /**
     * Machine readable count test.
     */
    @Test(dependsOnMethods = {"init"})
    public void machineReadableCountTest() {
        Assert.assertEquals(vdc.countDistributionsMachineReadable(), 4);
    }

    /**
     * Most used distribution formats test.
     */
    @Test(dependsOnMethods = {"init"})
    public void mostUsedDistributionFormatsTest() {
        Map<String, Long> map = vdc.listDistributionFormatsMostUsed(10);

        Assert.assertEquals(map.get("CSV").longValue(), 3);
        Assert.assertEquals(map.get("DOCX").longValue(), 3);
        Assert.assertEquals(map.get("TSV").longValue(), 1);
    }

    /**
     * Machine readable distribution count by catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void machineReadableDistributionCountByCatalogTest() {
        Map<Catalog, Long> map = vdc.listCatalogsWithMachineReadableDistributionsCount();

        for (Map.Entry<Catalog, Long> entry : map.entrySet()) {
            LOG.info("This is a catalog: {}", entry.getKey());
            switch (entry.getKey().getName()) {
                case CATALOG_NAME_NOT_OK:
                    Assert.assertEquals(map.get(entry.getKey()).longValue(), 4);
                    break;

                case CATALOG_NAME_OK:
                    Assert.assertEquals(map.get(entry.getKey()).longValue(), 0);
                    break;


                default:
                    Assert.fail();
            }
        }
    }

    /**
     * Distributions count per catalog test.
     */
    @Test(dependsOnMethods = {"init"})
    public void distributionsCountPerCatalogTest() {
        Map<String, Long> map = vdc.listCatalogIdsWithDistributionCount();
        Assert.assertTrue(map.keySet().size() == 2);
        Assert.assertEquals(map.values().toArray()[0], 7l);
    }

    /**
     * Available distributions count per catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void availableDistributionsCountPerCatalogTest() {
        Catalog catalog = vdc.getCatalogByName(CATALOG_NAME_NOT_OK);
        Assert.assertEquals(vdc.countDistributionsAvailableByCatalogId(catalog.getInstanceId()), 4l);
    }

    /**
     * Distribution status access codes of catalog test.
     */
    @Test(dependsOnMethods = {"init"})
    public void distributionStatusAccessCodesOfCatalogTest() {
        Catalog catalog = vdc.getCatalogByName(CATALOG_NAME_NOT_OK);
        Map<Integer, Long> map = vdc.listDistributionStatusCodesWithCountByCatalog(catalog);
        Assert.assertEquals(map.keySet().size(), 1);
        Assert.assertEquals(map.get(408).longValue(), 3l);
    }

    /**
     * Distributions with download url by catalog test.
     */
    @Test(dependsOnMethods = {"init"})
    public void distributionsWithDownloadUrlByCatalogTest() {
        Catalog catalog = vdc.getCatalogByName(CATALOG_NAME_NOT_OK);
        Assert.assertEquals(vdc.countDistributionsWithDownloadUrlByCatalog(catalog), 6);
    }

    /**
     * Machine readable distributions of catalog.
     */
    @Test(dependsOnMethods = {"init"})
    public void machineReadableDistributionsOfCatalogTest() {
        Catalog catalog = vdc.getCatalogByName(CATALOG_NAME_NOT_OK);
        Assert.assertEquals(vdc.countMachineReadableDistributionsByCatalog(catalog), 4);

        Catalog catalogOk = vdc.getCatalogByName(CATALOG_NAME_OK);
        Assert.assertEquals(vdc.countMachineReadableDistributionsByCatalog(catalogOk), 0);
    }

    @Test(dependsOnMethods = {"init"})
    public void machineReadableDatasetsOfCatalogTest() {
        Catalog catalog = vdc.getCatalogByName(CATALOG_NAME_NOT_OK);
        Assert.assertEquals(vdc.countMachineReadableDatasetsByCatalog(catalog), 2);

        Catalog catalogOk = vdc.getCatalogByName(CATALOG_NAME_OK);
        Assert.assertEquals(vdc.countMachineReadableDatasetsByCatalog(catalogOk), 0);
    }

    @Test(dependsOnMethods = {"init"})
    public void machineReadableDatasetCountByCatalogTest() {
        Map<Catalog, Long> map = vdc.listCatalogsWithMachineReadableDatasetsCount();
        Optional<Map.Entry<Catalog, Long>> catalogEntry = map.entrySet().stream().filter(catalog ->
                StringUtils.equals(catalog.getKey().getName(), CATALOG_NAME_NOT_OK)).findFirst();

        if (catalogEntry.isPresent()) {
            Assert.assertEquals((long) catalogEntry.get().getValue(), 2);
        } else {
            Assert.fail();
        }
    }

    @Test(dependsOnMethods = {"init"})
    public void machineReadableDatasetCountTest() {
        Assert.assertEquals(vdc.countMachineReadableDatasets(), 2);
    }

    @Test(dependsOnMethods = {"init"})
    public void licenceByIdTest() {
        Licence l = vdc.getLicenceById(LICENCE_NAME_KNOWN);
        Assert.assertNotNull(l);
        Assert.assertEquals(l.getLicenceId(), LICENCE_NAME_KNOWN);
    }

    @Test(dependsOnMethods = {"init"})
    public void allLicencesTest() {
        Assert.assertEquals(vdc.countAllLicences(), 2);
    }

    @Test(dependsOnMethods = {"init"})
    public void licenceCountTest() {
        Assert.assertEquals(vdc.countLicencesNonNullByCatalogName(CATALOG_NAME_NOT_OK), 2);
    }

    @Test(dependsOnMethods = {"init"})
    public void datasetsWithKnownLicencesCountTest() {
        Map<Catalog, Long> map = vdc.listCatalogsWithMostDatasetsOfKnownLicenceWithCount();
        map.forEach((catalog, count) -> {
            if (catalog.getName().equals(CATALOG_NAME_NOT_OK)) {
                Assert.assertEquals(count, Long.valueOf(1));
            } else {
                Assert.assertEquals(count, Long.valueOf(0));
            }
        });
    }

}
