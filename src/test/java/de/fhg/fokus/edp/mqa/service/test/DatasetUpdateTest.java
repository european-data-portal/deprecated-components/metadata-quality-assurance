package de.fhg.fokus.edp.mqa.service.test;

import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.persistence.entity.DistributionEntity;
import de.fhg.fokus.edp.mqa.service.test.util.ArquillianUtil;
import de.fhg.fokus.edp.mqa.service.test.util.EntityUtils;
import de.fhg.fokus.edp.mqa.service.test.util.TestConstants;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by bdi on 21/04/15.
 */
public class DatasetUpdateTest extends Arquillian {

    private static Logger LOG = LoggerFactory.getLogger(DatasetUpdateTest.class);

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient dc;

    @Inject
    private EntityUtils utils;

    /**
     * Create deployment web archive.
     *
     * @return the web archive
     */
    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ArquillianUtil.createWebArchive();
        LOG.info(war.toString(true));
        return war;
    }

    /**
     * Init.
     */
    @Test
    public void init() {
        Assert.assertNotNull(utils);
        utils.init();
    }

    /**
     * Update dataset.
     */
    @Test(dependsOnMethods = {"init"})
    public void updateDatasetTitleTest() {
        Assert.assertNotNull(dc.getDatasetByName(TestConstants.DATASET_NOT_OK));

        Dataset dataset = dc.getDatasetByName(TestConstants.DATASET_NOT_OK);
        Assert.assertTrue(dataset.getTitle().equals(TestConstants.DATASET_NOT_OK));

        final String newTitle = "My shiny new title!";
        dataset.setTitle(newTitle);
        Assert.assertTrue(dataset.getTitle().equals(newTitle));
        dc.updateEntity(dataset);
        Assert.assertTrue(dc.getDatasetByName(TestConstants.DATASET_NOT_OK).getTitle().equals(newTitle));
    }

    @Test(dependsOnMethods = {"init"})
    public void updateDistributionsOfDataset() {
        Assert.assertNotNull(dc.getDatasetByName(TestConstants.DATASET_OK));

        Dataset dataset = dc.getDatasetByName(TestConstants.DATASET_OK);
        Assert.assertEquals(dataset.getDistributions().size(), 3);
        dataset.getDistributions().clear();
        dc.updateEntity(dataset);

        Dataset datasetWithOutDist = dc.getDatasetByName(TestConstants.DATASET_OK);
        Assert.assertEquals(datasetWithOutDist.getDistributions().size(), 0);
    }

    @Test(dependsOnMethods = {"init"})
    public void checkDatasetMachineReadabilityTest() {
        Dataset dataset = dc.getDatasetByName(TestConstants.DATASET_NOT_OK);
        Assert.assertNotNull(dataset);
        Assert.assertTrue(dataset.isMachineReadable());

        dataset.getDistributions().clear();
        dataset = dc.updateEntity(dataset);
        Assert.assertFalse(dataset.isMachineReadable());

        Distribution distributionCsv = new DistributionEntity();
        distributionCsv.setAccessUrl("checkDatasetMachineReadabilityTest");
        distributionCsv.setDownloadUrl("checkDatasetMachineReadabilityTest");
        distributionCsv.setStatusAccessUrl(200);
        distributionCsv.setStatusDownloadUrl(200);
        distributionCsv.setFormat("csv");
        distributionCsv.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionCsv.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionCsv.setInstanceId(UUID.randomUUID().toString());

        dataset.getDistributions().add(dc.persistEntity(distributionCsv));
        dataset = dc.updateEntity(dataset);
        Assert.assertTrue(dataset.isMachineReadable());
    }
}
