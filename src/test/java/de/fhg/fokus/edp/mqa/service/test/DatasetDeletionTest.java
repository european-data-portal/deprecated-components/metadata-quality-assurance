package de.fhg.fokus.edp.mqa.service.test;

import de.fhg.fokus.edp.mqa.service.test.util.ArquillianUtil;
import de.fhg.fokus.edp.mqa.service.test.util.EntityUtils;
import de.fhg.fokus.edp.mqa.service.test.util.TestConstants;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.inject.Inject;

/**
 * Created by bdi on 21/04/15.
 */
public class DatasetDeletionTest extends Arquillian {

    private static Logger LOG = LoggerFactory.getLogger(DatasetDeletionTest.class);

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient dc;

    @Inject
    private EntityUtils utils;

    /**
     * Create deployment web archive.
     *
     * @return the web archive
     */
    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ArquillianUtil.createWebArchive();
        LOG.info(war.toString(true));
        return war;
    }

    /**
     * Delete dataset.
     */
    @Test
    public void deleteDatasetTest() {
        utils.init();
        Assert.assertNotNull(dc.getDatasetByName(TestConstants.DATASET_NOT_OK));

        int vCount = dc.listViolationNames().size();
        int dCount = dc.countAllDistributions();

        dc.removeEntity(dc.getDatasetByName(TestConstants.DATASET_NOT_OK));

        Assert.assertTrue(vCount > dc.listViolationNames().size());
        Assert.assertTrue(dCount > dc.countAllDistributions());

        Assert.assertNull(dc.getDatasetByName(TestConstants.DATASET_NOT_OK));
    }
}
