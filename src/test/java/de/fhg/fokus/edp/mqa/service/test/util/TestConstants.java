package de.fhg.fokus.edp.mqa.service.test.util;

/**
 * Created by bdi on 05/08/15.
 */
public interface TestConstants {
    /**
     * The constant CATALOG_NAME_NOT_OK.
     */
    String CATALOG_NAME_NOT_OK = "This catalog is not ok";
    /**
     * The constant CATALOG_NAME_OK.
     */
    String CATALOG_NAME_OK = "This catalog is ok";
    /**
     * The constant DATASET_OK.
     */
    String DATASET_OK = "dataset-ok";
    /**
     * The constant DATASET_NOT_OK.
     */
    String DATASET_NOT_OK = "dataset-not-ok";
    /**
     * The constant DATASET_OK_FOR_CATALOG_OK.
     */
    String DATASET_OK_FOR_CATALOG_OK = "Im OK";
    /**
     * The constant VIOLATION_NAME.
     */
    String VIOLATION_NAME = "ViolationEngine Name";

    String LICENCE_NAME_KNOWN = "test-licence-known";

    String LICENCE_NAME_UNKNOWN = "test-licence-unknown";
}
